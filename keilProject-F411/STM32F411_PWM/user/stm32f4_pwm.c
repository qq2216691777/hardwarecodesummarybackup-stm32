#include "stm32f4_pwm.h"
#include "stm32f4xx.h"
#include "stm32f4xx_tim.h"
/*
void PWM_Init( unsigned int arr )   arr   ms
GPIOB - 3
*/
void PWM_Init( unsigned int arr )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOB , ENABLE );
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2, ENABLE );
	
	GPIO_PinAFConfig( GPIOB, GPIO_PinSource3, GPIO_AF_TIM2 );
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseStructure.TIM_Period = arr;
	TIM_TimeBaseStructure.TIM_Prescaler = 100;
	
	TIM_TimeBaseInit( TIM2, &TIM_TimeBaseStructure );
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OC2Init( TIM2, &TIM_OCInitStructure );	
	
	TIM_OC2PreloadConfig( TIM2, TIM_OCPreload_Enable );
	TIM_ARRPreloadConfig( TIM2, ENABLE );
	TIM_Cmd( TIM2, ENABLE );
	
	TIM_SetCompare2( TIM2, 1 );  
	
}



/**
  ******************************************************************************
  *@file      main.c
  *@author    叶念西风
  *@version   2015.10.20
  *@brief     Template      
  *@HAUST - RobotLab           
  ******************************************************************************
**/
#include "stm32f4xx.h"

void LED_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	//GPIO_SetBits( GPIOA, GPIO_Pin_5 );
	//GPIO_ResetBits( GPIOA, GPIO_Pin_5 );
}


void Delay()
{
	u8 i=200;
	u16 j=2000;
	while(i--)
	{
		while(j--);
	}
}

int main( void )
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //设置系统中断优先级分组2
	
	LED_Init();
	while(1)
	{
		GPIO_ResetBits( GPIOA, GPIO_Pin_5 );
		Delay();
		GPIO_SetBits( GPIOA, GPIO_Pin_5 );
		Delay();
		
	}
}



#include "uda1380.h"

#define UDA_HW_ADDRESS         0x34    

__IO uint16_t  UDAAddress = 0;  

uint8_t play_record_mode;			//TRUE is play,FALSE is record mode.
uint16_t	audio_counter;			//Record counter.

uint16_t record_buf[60000];		//Record data buffer.

/********************************************************************
function: I2S_NVIC_Config(void)
discript: IIS interrupt init
entrance: none
return  : none
other   : More detial in stm32f4xx_it.c
********************************************************************/
void I2S_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	NVIC_InitStructure.NVIC_IRQChannel = SPI2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/********************************************************************
function: Codec_Init(void)
discript: Init Codec port and parameter
entrance: none
return  : none
other   : none
********************************************************************/
void Codec_Init(void)
{
	audio_counter = 0;
	play_record_mode = 0;		//Set to record mode.
	UDA_I2C_Init();
	I2S2_GPIO_Init();
	I2S_NVIC_Config();
	I2S_Cmd(SPI2, DISABLE);
	SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, DISABLE);
	SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, DISABLE);
	Codec_AudioInterface_Init_record(8000);
	
	RCC_PLLI2SCmd(ENABLE);
	//while (RCC_GetFlagStatus(RCC_FLAG_PLLI2SRDY) == RESET){}; //Wait for I2SPLL start


	I2S_Cmd(SPI2, ENABLE);
	



	UDA1380_Reg_Set(0x7f, 0x00, 0x00);		// Reset to L3 settings
	UDA1380_Reg_Set(0x02, 0xA5, 0x5F);		//analog mixer disable
	UDA1380_Reg_Set(0x00, 0x0D, 0x31);
	UDA1380_Reg_Set(0x01, 0x00, 0x50);
	UDA1380_Reg_Set(0x03, 0x00, 0x00);
	UDA1380_Reg_Set(0x04, 0x02, 0x02);
	
	UDA1380_Reg_Set(0x10, 0x00, 0x00);
	UDA1380_Reg_Set(0x11, 0x00, 0x00);
	UDA1380_Reg_Set(0x12, 0x55, 0x15);
	UDA1380_Reg_Set(0x13, 0x00, 0x00);
	UDA1380_Reg_Set(0x14, 0x00, 0x00);
	
	UDA1380_Reg_Set(0x20, 0x00, 0x00);
	UDA1380_Reg_Set(0x21, 0x00, 0x00);
	UDA1380_Reg_Set(0x22, 0x0F, 0x02);
	UDA1380_Reg_Set(0x23, 0x00, 0x00);

/*	
	UDA1380_Reg_Set(0x02, 0xA5, 0xDF);// Enable all power for now
	UDA1380_Reg_Set(0x00, 0x0F, 0x39);// CODEC ADC and DAC clock from WSPLL, all clocks enabled
	UDA1380_Reg_Set(0x01, 0x00, 0x00);// I2S bus data I/O formats, use digital mixer for output
	UDA1380_Reg_Set(0x03, 0x00, 0x37);// Full mixer analog input gain//default 0x37, 0x37
	UDA1380_Reg_Set(0x04, 0x02, 0x02);// Enable headphone short circuit protection
	
	UDA1380_Reg_Set(0x10, 0x00, 0x00);// Full master volume//control main vol 0x2f, 0x2f
	UDA1380_Reg_Set(0x11, 0xFF, 0x00);// Enable full mixer volume on both channels
	UDA1380_Reg_Set(0x12, 0xD4, 0x14);// Bass and treble boost set to flat
	UDA1380_Reg_Set(0x13, 0x02, 0x02);// Disable mute and de-emphasis
	UDA1380_Reg_Set(0x14, 0x00, 0x40);// Mixer off, other settings off
	
	UDA1380_Reg_Set(0x20, 0x1E, 0x1E);// ADC decimator volume to max
	UDA1380_Reg_Set(0x21, 0x00, 0x00);// No PGA mute, full gain
	UDA1380_Reg_Set(0x22, 0x00, 0x00);// Select line in and MIC, max MIC gain
	UDA1380_Reg_Set(0x23, 0x00, 0x00);// AGC
	//UDA1380_Reg_Set(0xff, 0xff, 0xff);// End of list


	UDA1380_Reg_Set(0x00, 0x0F, 0x39);
	UDA1380_Reg_Set(0x22, 0x00, 0x0f);
*/
	
	
}


/********************************************************************
function: UDA_I2C_Init(void)
discript: IIC port init,use common code
entrance: none
return  : none
other   : none
********************************************************************/
void UDA_I2C_Init(void)
{ 
	wolf_iic1_init();
  
  UDAAddress = UDA_HW_ADDRESS;  
}

/********************************************************************
function: UDA1380_Reg_Set(uint32_t reg, uint8_t Hi, uint8_t Lo)
discript: Set UDA1380 registor via IIC bus.
entrance: Reg address,High byte,low byte
return  : none
other   : As its different with common iic flow,so we recode this 
				  function
********************************************************************/
void UDA1380_Reg_Set(uint32_t reg, uint8_t Hi, uint8_t Lo)
{
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)){};

	  /*!< Send START condition */
  I2C_GenerateSTART(I2C1, ENABLE);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
	
	I2C_Send7bitAddress(I2C1, UDAAddress, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	
	I2C_SendData(I2C1, reg);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	I2C_SendData(I2C1, Hi);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	I2C_SendData(I2C1, Lo);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));	
	
	I2C_GenerateSTOP(I2C1, ENABLE);
}


void wolf_iic1_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure; 
	I2C_InitTypeDef I2C_InitStructure;
  
   
  /*!< sEE_I2C Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
  
  /*!< sEE_I2C_SCL_GPIO_CLK and sEE_I2C_SDA_GPIO_CLK Periph clock enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
//  RCC_AHB1PeriphClockCmd(sEE_I2C_SCL_GPIO_CLK | sEE_I2C_SDA_GPIO_CLK, ENABLE);

  //RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /* Reset sEE_I2C IP */
  RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, ENABLE);
  
  /* Release reset signal of sEE_I2C IP */
  RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, DISABLE);
    
  /*!< GPIO configuration */  
  /*!< Configure sEE_I2C pins: SCL */   
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init( GPIOB, &GPIO_InitStructure);

  /*!< Configure sEE_I2C pins: SDA */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Connect PXx to I2C_SCL*/
  GPIO_PinAFConfig( GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);

  /* Connect PXx to I2C_SDA*/
  GPIO_PinAFConfig( GPIOB, GPIO_PinSource7, GPIO_AF_I2C1);  
  
	
  /* sEE_I2C configuration */
  I2C_InitStructure.I2C_Mode =  I2C_Mode_I2C;
  I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
  I2C_InitStructure.I2C_OwnAddress1 = I2C_SLAVE_ADDRESS7;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  I2C_InitStructure.I2C_ClockSpeed = I2C_SPEED;
  
  /*!< I2C configuration */
  I2C_Init(I2C1, &I2C_InitStructure);
  
	I2C_AcknowledgeConfig(I2C1,ENABLE);
 
 /* sEE_I2C Peripheral Enable */
  I2C_Cmd(I2C1, ENABLE);
	  	
	I2C_AcknowledgeConfig(I2C1, ENABLE);
	
	I2C_ClearFlag(I2C1,I2C_FLAG_BUSY);
}


#define CODEC_I2S_WS_PIN    GPIO_Pin_13 //b
#define CODEC_I2S_SCK_PIN   GPIO_Pin_10 //b
#define CODEC_I2S_SD_PIN    GPIO_Pin_15 //b
#define CODEC_I2S_MCK_PIN   GPIO_Pin_6 //c

#define CODEC_I2S_WS_PINSRC    GPIO_PinSource13 //b
#define CODEC_I2S_SCK_PINSRC   GPIO_PinSource10 //b
#define CODEC_I2S_SD_PINSRC    GPIO_PinSource15 //b
#define CODEC_I2S_MCK_PINSRC   GPIO_PinSource6 //c

/********************************************************************
function: I2S_GPIO_Init(void)
discript: UDA1380 IIS port init
entrance: none
return  : none
other   : none
********************************************************************/
void I2S2_GPIO_Init(void)
{
   GPIO_InitTypeDef GPIO_InitStructure;
  
	/* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);  

  /* Enable I2S GPIO clocks */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC|RCC_AHB1Periph_GPIOB, ENABLE);  

  /* CODEC_I2S pins configuration: WS, SCK and SD pins -----------------------------*/
  GPIO_InitStructure.GPIO_Pin = CODEC_I2S_WS_PIN | CODEC_I2S_SCK_PIN | CODEC_I2S_SD_PIN; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStructure);     
  /* Connect pins to I2S peripheral  */
  GPIO_PinAFConfig(GPIOB, CODEC_I2S_WS_PINSRC, GPIO_AF_SPI2);  
  GPIO_PinAFConfig(GPIOB, CODEC_I2S_SCK_PINSRC, GPIO_AF_SPI2);
  GPIO_PinAFConfig(GPIOB, CODEC_I2S_SD_PINSRC, GPIO_AF_SPI2);

  /* CODEC_I2S pins configuration: MCK pin */
  GPIO_InitStructure.GPIO_Pin = CODEC_I2S_MCK_PIN; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_InitStructure);   
  /* Connect pins to I2S peripheral  */
  GPIO_PinAFConfig(GPIOC, CODEC_I2S_MCK_PINSRC, GPIO_AF_SPI2);    
}

/**
  * @brief  Initializes the Audio Codec audio interface (I2S)
  * @note   This function assumes that the I2S input clock (through PLL_R in 
  *         Devices RevA/Z and through dedicated PLLI2S_R in Devices RevB/Y)
  *         is already configured and ready to be used.    
  * @param  AudioFreq: Audio frequency to be configured for the I2S peripheral. 
  * @retval None
  */
void Codec_AudioInterface_Init_record(uint32_t AudioFreq)
{
  I2S_InitTypeDef I2S_InitStructure;

	I2S_Cmd( SPI2, DISABLE);

  /* Enable the CODEC_I2S peripheral clock */
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2, ENABLE);
	
  SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, DISABLE);
  SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, DISABLE);


  /* CODEC_I2S peripheral configuration */
  SPI_I2S_DeInit(SPI2);
  I2S_InitStructure.I2S_AudioFreq = AudioFreq;
  I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;
  I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_16b;
  I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;
  I2S_InitStructure.I2S_Mode = I2S_Mode_SlaveRx;

  I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Enable;

  /* Initialize the I2S peripheral with the structure above */
  I2S_Init(SPI2, &I2S_InitStructure);

  /* The I2S peripheral will be enabled only in the EVAL_AUDIO_Play() function 
       or by user functions if DMA mode not enabled */  
			 
	/* Enable the I2S2 RxE interrupt */
  SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, ENABLE);
	SPI_I2S_ClearFlag(SPI2,SPI_I2S_FLAG_RXNE);

	I2S_Cmd(SPI2, ENABLE);
}


void SPI2_IRQHandler(void)
{
	u16 ii;
	if(play_record_mode)	//play mode
	{											
		if (SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_TXE) == SET)		//Check data sent finished
		{	
			SPI_I2S_ClearFlag(SPI2,SPI_I2S_FLAG_TXE);							//Clear flag
			
			SPI_I2S_SendData(SPI2, record_buf[audio_counter]);		//Send next data
			audio_counter ++;
			if(audio_counter>= 50000)
			{
				audio_counter=0;
				printf("Play repead.\r\n");
			}
		}
	}
	else 	//record mode
	{
		if(SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_RXNE) == SET)		//Check data received							
		{			
			record_buf[audio_counter] =	SPI_I2S_ReceiveData(SPI2);//Read record data
			SPI_I2S_ClearFlag(SPI2,SPI_I2S_FLAG_RXNE);						//Clear received data flag
			audio_counter ++;
			if(audio_counter == 50000)
			{
				play_record_mode = 1;
//				audio_counter = 0;
//				Codec_AudioInterface_Init(I2S_AudioFreq_8k);				//Init to play mode
//				UDA1380_Reg_Set(0x00, 0x0F, 0x31);
				printf("Play record.\r\n");
				for(ii=0;ii<100;ii++)
				{
					printf("%x-", record_buf[ii]);
	
				}
			}
		}
	}
}




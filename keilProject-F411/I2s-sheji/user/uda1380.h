#ifndef _UDA1380_H___
#define _UDA1380_H___

#include "stm32f4xx.h"
#include "usart.h"
#include "delay.h"

#define I2C_SLAVE_ADDRESS7				0xA0		
#define I2C_SPEED                 100000

#define ADDR_16_BITS							1
#define ADDR_8_BITS								0

void Codec_Init(void);
void UDA_I2C_Init(void);
void wolf_iic1_init(void);
void I2S2_GPIO_Init(void);
void Codec_AudioInterface_Init_record(uint32_t AudioFreq);
void UDA1380_Reg_Set(uint32_t reg, uint8_t Hi, uint8_t Lo);

#endif


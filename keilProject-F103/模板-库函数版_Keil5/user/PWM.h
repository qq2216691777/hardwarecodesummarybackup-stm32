/**
  ***************************************
  *@FileName pwm.h
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Racing_Robot   
  ***************************************
**/
#ifndef _STM32_PWM_H__
#define _STM32_PWM_H__

void Tim2_PWM_Init( unsigned short arr, unsigned short psc );
void Tim3_PWM_Init( unsigned short arr, unsigned short psc );
void Tim4_PWM_Init( unsigned short arr, unsigned short psc );
void Tim5_PWM_Init( unsigned short arr, unsigned short psc );

#endif

/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@brief              
  *@HAUST - RobotLab
  ***************************************
**/


#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "LED.h"

int main( void )
{
	delay_init();
	NVIC_Configuration(); //�����жϷ���
	uart_init( 115200 );
	LED_Configuration();
	while(1)
	{
		 LED_OFF();
		delay_ms(500);
		 LED_ON();
		delay_ms(500);
	}
}


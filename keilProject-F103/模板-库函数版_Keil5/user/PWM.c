/**
  ***************************************
  *@FileName pwm.h
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Racing_Robot Config PWM Signal
  ***************************************
**/
#include "pwm.h"
#include "stm32f10x.h"                  // Device header

void Tim2_PWM_Init( unsigned short arr, unsigned short psc )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2, ENABLE );	//时钟使能
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE );
	
	GPIO_PinRemapConfig( GPIO_Remap_SWJ_JTAGDisable, ENABLE );		//关闭JTAG功能 释放相应引脚
	GPIO_PinRemapConfig( GPIO_FullRemap_TIM2, ENABLE );		//打开定时器引脚重定义功能
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15 ;	//配置PWM输出引脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_10 | GPIO_Pin_11;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
	TIM_TimeBaseStructure.TIM_Period = arr-1;		//定时器配置
	TIM_TimeBaseStructure.TIM_Prescaler = psc-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseInit( TIM2, &TIM_TimeBaseStructure );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init( TIM2, &TIM_OCInitStructure );
	TIM_OC1PreloadConfig( TIM2, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC2Init( TIM2, &TIM_OCInitStructure );
	TIM_OC2PreloadConfig( TIM2, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC3Init( TIM2, &TIM_OCInitStructure );
	TIM_OC3PreloadConfig( TIM2, TIM_OCPreload_Enable );
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC4Init( TIM2, &TIM_OCInitStructure );
	TIM_OC4PreloadConfig( TIM2, TIM_OCPreload_Enable );
	
	TIM_Cmd( TIM2, ENABLE );
	
}


void Tim3_PWM_Init( unsigned short arr, unsigned short psc )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, ENABLE );	//时钟使能
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE );
	
	GPIO_PinRemapConfig( GPIO_FullRemap_TIM3, ENABLE );		//打开定时器引脚重定义功能
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 ;	//配置PWM输出引脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOC, &GPIO_InitStructure );
	
	TIM_TimeBaseStructure.TIM_Period = arr-1;		//定时器配置
	TIM_TimeBaseStructure.TIM_Prescaler = psc-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseInit( TIM3, &TIM_TimeBaseStructure );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init( TIM3, &TIM_OCInitStructure );
	TIM_OC1PreloadConfig( TIM3, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC2Init( TIM3, &TIM_OCInitStructure );
	TIM_OC2PreloadConfig( TIM3, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC3Init( TIM3, &TIM_OCInitStructure );
	TIM_OC3PreloadConfig( TIM3, TIM_OCPreload_Enable );
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC4Init( TIM3, &TIM_OCInitStructure );
	TIM_OC4PreloadConfig( TIM3, TIM_OCPreload_Enable );
	
	TIM_Cmd( TIM3, ENABLE );
	
}


void Tim4_PWM_Init( unsigned short arr, unsigned short psc )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM4, ENABLE );	//时钟使能
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB , ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 ;	//配置PWM输出引脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
	TIM_TimeBaseStructure.TIM_Period = arr-1;		//定时器配置
	TIM_TimeBaseStructure.TIM_Prescaler = psc-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseInit( TIM4, &TIM_TimeBaseStructure );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init( TIM4, &TIM_OCInitStructure );
	TIM_OC1PreloadConfig( TIM4, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC2Init( TIM4, &TIM_OCInitStructure );
	TIM_OC2PreloadConfig( TIM4, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC3Init( TIM4, &TIM_OCInitStructure );
	TIM_OC3PreloadConfig( TIM4, TIM_OCPreload_Enable );
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC4Init( TIM4, &TIM_OCInitStructure );
	TIM_OC4PreloadConfig( TIM4, TIM_OCPreload_Enable );
	
	TIM_Cmd( TIM4, ENABLE );
	
}
void Tim5_PWM_Init( unsigned short arr, unsigned short psc )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM5, ENABLE );	//时钟使能
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 ;	//配置PWM输出引脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	TIM_TimeBaseStructure.TIM_Period = arr-1;		//定时器配置
	TIM_TimeBaseStructure.TIM_Prescaler = psc-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseInit( TIM5, &TIM_TimeBaseStructure );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init( TIM5, &TIM_OCInitStructure );
	TIM_OC1PreloadConfig( TIM5, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC2Init( TIM5, &TIM_OCInitStructure );
	TIM_OC2PreloadConfig( TIM5, TIM_OCPreload_Enable );
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC3Init( TIM5, &TIM_OCInitStructure );
	TIM_OC3PreloadConfig( TIM5, TIM_OCPreload_Enable );
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC4Init( TIM5, &TIM_OCInitStructure );
	TIM_OC4PreloadConfig( TIM5, TIM_OCPreload_Enable );
	
	TIM_Cmd( TIM5, ENABLE );
	
}



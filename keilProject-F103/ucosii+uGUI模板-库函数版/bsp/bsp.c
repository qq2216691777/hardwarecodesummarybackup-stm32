#include "INCLUDES.H"

void BSP_Init( void )
{
	//SystemInit();			/*配置系统时钟为72M 系统自动调用*/	
	NVIC_Configuration();
}

void SysTick_Init( void )
{
	SysTick_Config( SystemCoreClock/OS_TICKS_PER_SEC);
}

void LED_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOD, &GPIO_InitStructure ); 
	
}

void LED2_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOG, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOG, &GPIO_InitStructure ); 
	
}


#ifndef __INCLUDES_H__
#define __INCLUDES_H__

#include "stm32f10x.h"
#include "usart.h"
#include "ucos_ii.h"
#include "bsp.h"
#include "app.h"
#include "gui.h"
#include "gui_x.h"
#include "wm.h"
#include "FrameWin.h"
#include "lcd_touch.h"
#include "button.h"

#endif

#include "includes.h"

OS_STK TASK_LED_STK[TASK_LED_STK_SIZE];
OS_STK TASK_LED2_STK[TASK_LED2_STK_SIZE];
OS_STK TASK_USART_STK[TASK_USART_STK_SIZE];
OS_STK TASK_LCD_STK[TASK_LCD_STK_SIZE];

void Task_Start( void *p_arg )		//开始任务
{
	p_arg = p_arg;
	SysTick_Init();
	
	#if(OS_TASK_STAT_EN > 0)
	OSStatInit();	//	统计任务初始化函数
	#endif
	GUI_Init();
	GUI_SetBkColor( GUI_WHITE );
	GUI_Clear();
	GUI_CURSOR_Show();
	OSTaskCreate( Task_LCD, (void *)0,&TASK_LCD_STK[TASK_LCD_STK_SIZE-1],TASK_LCD_PRIO);
//	OSTaskCreate( Task_LED, (void *)0,&TASK_LED_STK[TASK_LED_STK_SIZE-1],TASK_LED_PRIO);
	OSTaskCreate( Task_LED2, (void *)0,&TASK_LED2_STK[TASK_LED2_STK_SIZE-1],TASK_LED2_PRIO);
//	OSTaskCreate( Task_Usart, (void *)0,&TASK_USART_STK[TASK_USART_STK_SIZE-1],TASK_USART_PRIO);
//	GUI_Exec();
	while(1)
	{
		OSTimeDlyHMSM(0,0,0,20);
		GUI_TOUCH_Exec();
		
		
	}
	
}

/*//任务模板
void Task_( void *p_arg )
{
	p_arg = p_arg;
	
	while(1)
	{

	}
	
}
*/

void Task_LED( void *p_arg)			//LED1任务
{
	GUI_PID_STATE Button_State;
	p_arg = p_arg;
	LED_Init();
	GPIO_SetBits(GPIOD,GPIO_Pin_13);
	
	while(1)
	{
		OSTimeDlyHMSM(0,0,0,10);	
//		GUI_CURSOR_Show();
		
//		GUI_TOUCH_StoreState();
	}
}

void Task_LED2( void *p_arg )		//LED2任务
{
	p_arg = p_arg;
	LED2_Init();
	while(1)
	{
		GPIO_SetBits(GPIOG,GPIO_Pin_14);
		OSTimeDlyHMSM(0,0,0,70);
		GPIO_ResetBits(GPIOG,GPIO_Pin_14);
		OSTimeDlyHMSM(0,0,2,0);
	}
}

void Task_Usart( void *p_arg )
{
	p_arg = p_arg;
	uart_init(115200);
	while(1)
	{	
		if( USART_RX_STA&0x8000 )
		{
			printf("SUCCESSED\n");
			USART_RX_STA=0;
		}   
		OSTimeDlyHMSM(0,0,0,10);
	}
	
}

void Task_LCD( void *p_arg )
{
	int i;
	u8 key;
	BUTTON_Handle bt1,bt2,bt3,bt4;
	FRAMEWIN_Handle ft;
	WM_HWIN hWin1;
	GUI_PID_STATE Button_State;
	p_arg = p_arg;
//	WM_Activate();
	
	bt1 = BUTTON_Create( 40,40,70,50,50,WM_CF_SHOW);
	BUTTON_SetText( bt1, "LED_ON" );
	
	bt2 = BUTTON_Create( 120,40,70,50,51,WM_CF_SHOW);
	BUTTON_SetText( bt2, "LED_OFF" );
	
	bt3 = BUTTON_Create( 40,100,150,50,52,WM_CF_SHOW);
	BUTTON_SetText( bt3, "TEXT" );
	
	ft = FRAMEWIN_Create( "TEXT",0,WM_CF_HIDE,0,0,240,320 );
	bt4 = BUTTON_CreateAsChild( 180,250,50,40,ft,53,WM_CF_SHOW);
	BUTTON_SetText( bt4, "EXIT" );
	
	LED_Init();
	GUI_SetBkColor( GUI_WHITE );
	
	while(1)
	{
	
		GUI_SetColor(GUI_BLACK);
		for(i=1;i<80;i+=5)
		{		
			GUI_TOUCH_GetState(&Button_State);
			
			GUI_DrawCircle(120,235,i);
			OSTimeDlyHMSM(0,0,0,100);
			WM_Exec();
		//	GUI_X_WAIT_EVENT();
				key = GUI_GetKey();
				switch(key)
				{
					case 50: GPIO_SetBits(GPIOD,GPIO_Pin_13); break;
					case 51: GPIO_ResetBits(GPIOD,GPIO_Pin_13); break;
					case 52: WM_ShowWindow(ft); break;
					case 53: WM_HideWindow(ft);  GUI_Clear(); break;
					default : break;
				}
		}
		GUI_SetColor(GUI_WHITE);
		GUI_FillRect(30,140,210,315);
	}
	
}



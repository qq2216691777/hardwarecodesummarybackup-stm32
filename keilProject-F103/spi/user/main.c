#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "spi_nrf.h"

u8 NRF_Status;
u8 Rxbuf[4];
u8 Txbuf[4];
int i=0;
u16 ii;

int main( void )
{
	delay_init();
	NVIC_Configuration();
	uart_init( 9600 );
	SPI2_NRF_Init();		//	初始化spi 
	printf("这是NRF24L01_SPI测试程序...\r\n");
	NRF_Status = NRF_Check();
	if( NRF_Status )
	{
		printf("\r\nNRF与MCU连接成功！\r\n");
	}
	else
	{
	    printf("\r\n  NRF与MCU连接失败，请重新检查接线。\r\n");
	}
	ii=0;
	printf("\r\n 主机端 进入接收模式。 \r\n");
	while(1)
	{
			
	    NRF_RX_Mode();
	 	/*等待接收数据*/
    	NRF_Status = NRF_Rx_Dat(Rxbuf);
		/*判断接收状态*/
	    switch(NRF_Status)
	    {
	      case RX_DR:
	      for(i=0;i<RX_PLOAD_WIDTH;i++)
	      {					
	        printf("\r\n 主机端 接收到 从机端 发送的数据为：%d \r\n",Rxbuf[i]);
	        Txbuf[i] =Rxbuf[i];
	      }
	      break;
	
	      case ERROR:
		  	ii++;
			if(ii==8500)
			{
				ii=0;
	        	printf("\r\n 主机端 接收出错。   \r\n");
			}
	      break;  		
	    }
	}
}



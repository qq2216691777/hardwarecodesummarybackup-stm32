#ifndef _TLC2543_H__
#define _TLC2543_H__

#include "stm32f10x.h"
#include "delay.h"

void TLC2543_Init( void );
u16 TLC2543_Read( u16 reg );


#define TLC_CS PAout(2)
#define TLC_CLK PAout(5)
#define TLC_OUT PAin(4)
#define TLC_IN PAout(1)

#endif

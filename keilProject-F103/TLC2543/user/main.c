/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     工程模板   
			@日期     2015—6-23
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "tlc2543.h"

int main( void )
{
	vu32 da;
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	TLC2543_Init();
	
	while(1)
	{
		da = TLC2543_Read(0x0C);
		printf("%d\n",da);
		delay_ms(100);
	}
}


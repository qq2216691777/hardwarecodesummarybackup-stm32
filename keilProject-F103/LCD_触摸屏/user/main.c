/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@brief              
  *@HAUST - RobotLab
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "ili9320.h"
#include "lcd_touch.h"

int main( void )
{
	u32 tick=0;
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Ili9320_Init(); 
  Lcd_Touch_Init();	
  LCD_Clear( Red );
	
// 	ili9320_SetWindows(0,50,100,150);
// 	LCD_WriteRAM_Prepare();
// 	tick = 100*100;
// 	while(tick--)
// 	{
// 		delay_ms(1);
// 		LCD->LCD_RAM=Black;
// 	}
// 	while(1);
	GUI_Text( 0,80,"Zhou Wangsheng", Black, White );
	while(1)
	{
		tick++;
		if( tick>100000 )
		{
			tick=0;
			GUI_Word(150,120,5,TOUCH_X(),0,White,Red);	 //X坐标
			GUI_Word(150,150,5,TOUCH_Y(),0,White,Red);	 //Y坐标
		}
	}	 
}


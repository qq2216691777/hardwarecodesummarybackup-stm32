
/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@brief              
  *@HAUST - RobotLab
  ***************************************
**/

#include "ili9320.h"
#include "ili9320_font.h"
#include "string.h"

void Ili9320_Init( void )
{
	u16 DeviceCode;
	u16 i;
	LCD_Init();
	FSMC_Init();
	delay_ms(50);
	DeviceCode = LCD_ReadReg(0x0000);
	while( (DeviceCode != 0x9328) && (DeviceCode != 0x9325) )
	{
		DeviceCode = LCD_ReadReg(0x0000);
		//printf("LCD初始化失败\r\n");
		delay_ms(100);
	}
	
	LCD_WriteReg(0x00e7,0x0010);
	LCD_WriteReg(0x0000,0x0001);  			     //start internal osc
	
/*********************************************************************/	
	#if _XTOY_OPEN_==0
	LCD_WriteReg(0x0001,0x0100); 
	LCD_WriteReg(0x0002,0x0700); 				 //power on sequence   
	LCD_WriteReg(0x0003,(1<<12)|(1<<5)|(1<<4)|(0<<3) ); //65K  竖 (1<<12)|(1<<5)|(1<<4)|(0<<3) 
	#else
	LCD_WriteReg(0x0001,0x0000);
	LCD_WriteReg(0x0002,0x0700); 				 //power on sequence   
	LCD_WriteReg(0x0003,(1<<12)|(1<<5)|(1<<4)|(1<<3) ); //65K  竖 (1<<12)|(1<<5)|(1<<4)|(0<<3) 
	#endif
	
	/*************************************************/
	
                   
	LCD_WriteReg(0x0004,0x0000);                                   
	LCD_WriteReg(0x0008,0x0202);				//Display Contral 2.(0x0207)	           
	LCD_WriteReg(0x0009,0x0000);         
	LCD_WriteReg(0x000a,0x0000); 				 //display setting         
	LCD_WriteReg(0x000c,0x0001);				 //display setting          
	LCD_WriteReg(0x000d,0x0000); 				 //0f3c          
	LCD_WriteReg(0x000f,0x0000);
	//Power On sequence //
	LCD_WriteReg(0x0010,0x0000);   
	LCD_WriteReg(0x0011,0x0007);
	LCD_WriteReg(0x0012,0x0000);                                                                 
	LCD_WriteReg(0x0013,0x0000); 
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0010,0x1590);   
	LCD_WriteReg(0x0011,0x0227);
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0012,0x009c);   
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0013,0x1900);   
	LCD_WriteReg(0x0029,0x0023);
	LCD_WriteReg(0x002b,0x000e);
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0020,0x0000);                                                            
	LCD_WriteReg(0x0021,0x0000);           
	
	///////////////////////////////////////////////////////      
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0030,0x0007); 
	LCD_WriteReg(0x0031,0x0707);   
	LCD_WriteReg(0x0032,0x0006);
	LCD_WriteReg(0x0035,0x0704);
	LCD_WriteReg(0x0036,0x1f04); 
	LCD_WriteReg(0x0037,0x0004);
	LCD_WriteReg(0x0038,0x0000);        
	LCD_WriteReg(0x0039,0x0706);     
	LCD_WriteReg(0x003c,0x0701);
	LCD_WriteReg(0x003d,0x000f);
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0050,0x0000);       //X起始地址 
	LCD_WriteReg(0x0051,0x00ef);   		 //X终止地址
	LCD_WriteReg(0x0052,0x0000);     	 //Y起始地址 
	LCD_WriteReg(0x0053,0x013f);			 //Y终止地址
	LCD_WriteReg(0x0060,0xa700);        
	LCD_WriteReg(0x0061,0x0001); 
	LCD_WriteReg(0x006a,0x0000);
	LCD_WriteReg(0x0080,0x0000);
	LCD_WriteReg(0x0081,0x0000);
	LCD_WriteReg(0x0082,0x0000);
	LCD_WriteReg(0x0083,0x0000);
	LCD_WriteReg(0x0084,0x0000);
	LCD_WriteReg(0x0085,0x0000);  
	LCD_WriteReg(0x0090,0x0010);     
	LCD_WriteReg(0x0092,0x0000);  
	LCD_WriteReg(0x0093,0x0003);
	LCD_WriteReg(0x0095,0x0110);
	LCD_WriteReg(0x0097,0x0000);        
	LCD_WriteReg(0x0098,0x0000);  
	//display on sequence     
	LCD_WriteReg(0x0007,0x0133);
	LCD_WriteReg(0x0020,0x0000);                                                            
	LCD_WriteReg(0x0021,0x0000);
	delay_ms(50);
}

void LCD_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE |
                         RCC_APB2Periph_GPIOF | RCC_APB2Periph_GPIOG |
                         RCC_APB2Periph_AFIO, ENABLE);
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_FSMC, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_SetBits(GPIOA, GPIO_Pin_1);						//打开背光  点亮Lcd 

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5 |
	                            GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_14 | 
	                            GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | 
	                            GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | 
	                            GPIO_Pin_15;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	/* Set PF.00(A0 (RS)) as alternate function push pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOF, &GPIO_InitStructure);

	/* Set PG.12(NE4 (LCD/CS)) as alternate function push pull - CE3(LCD /CS) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_Init(GPIOG, &GPIO_InitStructure);
}

void FSMC_Init( void )
{
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
	FSMC_NORSRAMTimingInitTypeDef  p;
	
	/*-- FSMC Configuration ------------------------------------------------------*/
	/*----------------------- SRAM Bank 4 ----------------------------------------*/
	/* FSMC_Bank1_NORSRAM4 configuration */
	p.FSMC_AddressSetupTime = 0;
	p.FSMC_AddressHoldTime = 0;
	p.FSMC_DataSetupTime = 10;
	p.FSMC_BusTurnAroundDuration = 0;
	p.FSMC_CLKDivision = 0;
	p.FSMC_DataLatency = 0;
	p.FSMC_AccessMode = FSMC_AccessMode_A;
	
	/* Color LCD configuration ------------------------------------
	 LCD configured as follow:
	    - Data/Address MUX = Disable
	    - Memory Type = SRAM
	    - Data Width = 16bit
	    - Write Operation = Enable
	    - Extended Mode = Enable
	    - Asynchronous Wait = Disable 	*/
	FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM4;
	FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait=FSMC_AsynchronousWait_Disable; 
	FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
//	FSMC_NORSRAMInitStructure.FSMC_AsyncWait = FSMC_AsyncWait_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &p;
	FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &p;
	
	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);  
	
	/* BANK 4 (of NOR/SRAM Bank 1~4) is enabled */
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM4, ENABLE);
	delay_ms(50);
}

u16 LCD_ReadReg( u8 LCD_Reg)
{
	LCD->LCD_REG = LCD_Reg;
	return (LCD->LCD_RAM);
}

void LCD_WriteReg(u8 LCD_Reg,u16 LCD_RegValue)
{
  /* Write 16-bit Index, then Write Reg */
  LCD->LCD_REG = LCD_Reg;
  /* Write 16-bit Reg */
  LCD->LCD_RAM = LCD_RegValue;
}

void LCD_Clear( u16 Color )
{
  u32 index=0;
// 	ili9320_SetCursor(0,0);
// 	ili9320_SetWindows(50,50,200,200);
// 	LCD_WriteRAM_Prepare();
	ili9320_SetCursor(0,0); 
  LCD_WriteRAM_Prepare(); /* Prepare to write GRAM */
  for(index=0;index<76800;index++)
   {
//		 delay_us(900);
     LCD->LCD_RAM=Color;
   }
}


/****************************************************************************
* 名    称：void ili9320_SetCursor(u16 x,u16 y)
* 功    能：设置屏幕座标
* 入口参数：x      行座标
*           y      列座标
* 出口参数：无
* 说    明：
* 调用方法：ili9320_SetCursor(10,10);
****************************************************************************/
__inline void ili9320_SetCursor(u16 x,u16 y)
{
	LCD_WriteReg(0x0020,x);        //行x
	LCD_WriteReg(0x0021,y);        //列	0x13f-y
}

void LCD_WriteRAM_Prepare(void)
{
  LCD->LCD_REG = R34;
}

void ili9320_PutChar(u16 x,u16 y,u8 c,u16 charColor,u16 bkColor)
{
  u16 i=0;
  u16 j=0;
  u8 tmp_char=0;	  //获得字符c的ascll码值
	ili9320_SetWindows(x,y,x+8,y+16);
	LCD_WriteRAM_Prepare();
	for(i=0;i<16;i++)
	{
		tmp_char=ascii_8x16[((c-0x20)*16)+i];
		for(j=0;j<8;j++)
		{
			if ( ( tmp_char >> 7-j ) & 0x01 == 0x01)
			{
				LCD->LCD_RAM = charColor; // 字符颜色
			}
			else
			{
				LCD->LCD_RAM=bkColor; // 背景颜色
			}
		}
	}

}


/****************************************************************************
* 名    称：void GUI_Text(u16 x, u16 y, u8 *str, u16 len,u16 Color, u16 bkColor)
* 功    能：在指定座标显示字符串
* 入口参数：x      行座标
*           y      列座标
*           *str   字符串
*           len    字符串长度
*           Color  字符颜色
*           bkColor字符背景颜色
* 出口参数：无
* 说    明：
* 调用方法：GUI_Text(0,0,"0123456789",10,0x0000,0xffff);
****************************************************************************/
void GUI_Text(u16 x, u16 y, char *str, u16 Color, u16 bkColor)
{
  u8 i;
  u16 len;
	len = strlen(str);
  for (i=0;i<len;i++)
  {
    ili9320_PutChar(( x+8*i ), y, *str++, Color, bkColor);
  }
}

void ili9320_SetPoint(u16 x,u16 y,u16 point)
{
  if ( (x>320)||(y>240) ) return;
  ili9320_SetCursor(x,y);

  LCD_WriteRAM_Prepare();
  LCD_WriteRAM(point);
}

void LCD_WriteRAM(u16 RGB_Code)					 
{
  /* Write 16-bit GRAM Reg */
  LCD->LCD_RAM = RGB_Code;
}

/****************************************************************************
* 名    称：u16 ili9320_GetPoint(u16 x,u16 y)
* 功    能：获取指定座标的颜色值
* 入口参数：x      行座标
*           y      列座标
* 出口参数：当前座标颜色值
* 说    明：
* 调用方法：i=ili9320_GetPoint(10,10);
****************************************************************************/
u16 ili9320_GetPoint(u16 x,u16 y)
{
  ili9320_SetCursor(x,y);
  return (ili9320_BGR2RGB(LCD_ReadRAM()));
}

/****************************************************************************
* 名    称：u16 ili9320_BGR2RGB(u16 c)
* 功    能：RRRRRGGGGGGBBBBB 改为 BBBBBGGGGGGRRRRR 格式
* 入口参数：c      BRG 颜色值
* 出口参数：RGB 颜色值
* 说    明：内部函数调用
* 调用方法：
****************************************************************************/
u16 ili9320_BGR2RGB(u16 c)
{
  u16  r, g, b, rgb;

  b = (c>>0)  & 0x1f;
  g = (c>>5)  & 0x3f;
  r = (c>>11) & 0x1f;
  
  rgb =  (b<<11) + (g<<5) + (r<<0);

  return( rgb );
}

/*******************************************************************************
* Function Name  : LCD_ReadRAM
* Description    : Reads the LCD RAM.
* Input          : None
* Output         : None
* Return         : LCD RAM Value.
*******************************************************************************/
u16 dummy;
u16 LCD_ReadRAM(void)
{
  
  /* Write 16-bit Index (then Read Reg) */
  LCD->LCD_REG = R34; /* Select GRAM Reg */
  /* Read 16-bit Reg */
  dummy = LCD->LCD_RAM; 
  return dummy;
}
#if _CHINESE_OPEN_ == 1
	/****************************************************************************
	* 名    称：void GUI_Text(u16 x, u16 y, u8 *str, u16 len,u16 Color, u16 bkColor)
	* 功    能：在指定座标显示汉字文本
	* 入口参数：x      行座标
	*           y      列座标
	*           *str   汉字串
	*           len    字串长度
	*           Color  字符颜色
	*           bkColor字符背景颜色
	* 出口参数：无
	* 说    明：一个汉字两个字符，记得哟，5个汉字len就是10
	* 调用方法：GUI_Text(0,0,"成都贝一特",10,0x0000,0xffff);
	****************************************************************************/
	void GUI_Chinese_Text(u16 x,u16 y, char str[],u16 charColor,u16 bkColor)
	{
		u16 i=0,b;
		u16 j=0;
		u16 x_add,y_add;
		u16 tmp_char=0,index=0;
		u8 len;
		x_add=x;
		y_add=y;
		len = strlen(str);
		for(b=0;b<len/2;b++)
		{
				index=(94*(str[b*2] - 0xa1)+(str[b*2+1] - 0xa1));	    //计算区位
				for (i=0;i<16;i++)
					{
						for (j=0;j<8;j++)
						{
							tmp_char=HzLib[index*32+i*2];					//查字库
								if ( (tmp_char << j) & 0x80)
								{
									ili9320_SetPoint(x_add+j,y_add+i,charColor);  // 字符颜色
								}
								else
								{
									ili9320_SetPoint(x_add+j,y_add+i,bkColor);   // 背景颜色
								}
						}
						for (j=0;j<8;j++)
						{
							tmp_char=HzLib[index*32+i*2+1];				    //查字库
								if ( (tmp_char << j) & 0x80)
								{
									ili9320_SetPoint(x_add+j+8,y_add+i,charColor); // 字符颜色
								}
								else
								{
									ili9320_SetPoint(x_add+j+8,y_add+i,bkColor);   // 背景颜色
								}
						}
					 }
				 x_add=x_add+17;										 //地址累加
		}
	}
#endif

/*******************************************************************************
函数名     ： DispWord
功能       ： 显示9位以下数字，最大显示999999999
参数       ： x     : 坐标X
              y     : 坐标Y
			  maxFb : 要显示的位数，注：小于要显示的位数前面补0。
			  Da    : 要显示的数据	
			  Point : 小数点的位数  
返回值     ： 无
*******************************************************************************///
void GUI_Word(u16  x, u16  y,u16  maxXS, u32  Da,  u16 Point, u16 Color, u16 bkColor)
{
 	u8  i;//,Fb;
    
	u32 NUM;
	NUM=1;
 	for(i=0; i<(maxXS-1); i++)  NUM=NUM*10;//留做要显示的位数取位用  
	if(Point!=0)ili9320_PutChar((x+(maxXS -Point)*8),y,0x2e,Color,bkColor);//如果小数点位数非零显示小数点
	
	for(i=0; i<(maxXS-0); i++)  //从最高位依次显示要显示的位数
	{   
		if(i<=(maxXS-Point-1))  //如果该位是小数点所在的位以前的位（包括小数点所在的位）则正常显示
		{
		   ili9320_PutChar((x+i*8),y,((Da/NUM)%10+0x30),Color,bkColor);
		   NUM=NUM/10;
                   
		}
		else                    //否则显示数字移位一位数字用来显示小数点
		{
		   ili9320_PutChar((x+i*8+8),y,((Da/NUM)%10+0x30),Color,bkColor);
		   NUM=NUM/10;
		} 
	}	
	return;
}

/****************************************************************************
* 名    称：void ili9320_SetWindows(u16 StartX,u16 StartY,u16 EndX,u16 EndY)
* 功    能：设置窗口区域
* 入口参数：StartX     行起始座标
*           StartY     列起始座标
*           EndX       行结束座标
*           EndY       列结束座标
* 出口参数：无
* 说    明：
* 调用方法：ili9320_SetWindows(0,0,100,100)；
****************************************************************************/
void ili9320_SetWindows( u16 StartX,u16 StartY,u16 EndX,u16 EndY)
{
 	#if _XTOY_OPEN_ == 0
  ili9320_SetCursor(StartX,StartY);
  LCD_WriteReg(0x0050, StartX);
  LCD_WriteReg(0x0052, StartY);
  LCD_WriteReg(0x0051, EndX-1);
  LCD_WriteReg(0x0053, EndY-1);
	#else
	ili9320_SetCursor(StartY,StartX);
  LCD_WriteReg(0x0050, StartY);
  LCD_WriteReg(0x0052, StartX);
  LCD_WriteReg(0x0051, EndY-1);
  LCD_WriteReg(0x0053, EndX-1);
	#endif
}


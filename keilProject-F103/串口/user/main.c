#include "system_start.h"
#include "uart.h"
#include "nvic.h"


int main( void )
{
		u16 Len;
		u16 Len_n;
		Stm32_Clock_Init( 9 );
		Uart_Init( 115200, 0 );
	
		while(1)
		{
				if(	USART1_RX_STA & 0x8000 )
				{
						Len = USART1_RX_STA & 0X3fff;
						printf("串口1本次接收到数据是:\n");
						for( Len_n = 0; Len_n<Len; Len_n++ )
						{
								USART1->DR = USART1_RX_BUF[Len_n];
								while( (USART1->SR&0X40)==0 );
						}
						printf("\n");
						USART1_RX_STA = 0;					
				}
		}	
}



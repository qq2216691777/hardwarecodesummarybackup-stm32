#include "sys.h"
#include "mpu6050_iic_sim.h"
#include "kalman.h"




////卡尔曼滤波参数与函数
//float dt=0.005;//注意：dt的取值为kalman滤波器采样时间
//float angle_kalmen, angle_dot;//角度和角速度
//float P[2][2] = {{ 1, 0 },
//              { 0, 1 }};
//float Pdot[4] ={ 0,0,0,0};
//float Q_angle=0.001, Q_gyro=0.005; //角度数据置信度,角速度数据置信度
//float R_angle=0.5 ,C_0 = 1; 
//float q_bias, angle_err, PCt_0, PCt_1, E, K_0, K_1, t_0, t_1;


////卡尔曼滤波
//float Kalman_Filter(float angle_m, float gyro_m)//angleAx 和 gyroGy
//{
//angle_kalmen+=(gyro_m-q_bias) * dt;
//angle_err = angle_m - angle_kalmen;
//Pdot[0]=Q_angle - P[0][1] - P[1][0];
//Pdot[1]=-P[1][1];
//Pdot[2]=-P[1][1];
//Pdot[3]=Q_gyro;
//P[0][0] += Pdot[0] * dt;
//P[0][1] += Pdot[1] * dt;
//P[1][0] += Pdot[2] * dt;
//P[1][1] += Pdot[3] * dt;
//PCt_0 = C_0 * P[0][0];
//PCt_1 = C_0 * P[1][0];
//E = R_angle + C_0 * PCt_0;
//K_0 = PCt_0 / E;
//K_1 = PCt_1 / E;
//t_0 = PCt_0;
//t_1 = C_0 * P[0][1];
//P[0][0] -= K_0 * t_0;
//P[0][1] -= K_0 * t_1;
//P[1][0] -= K_1 * t_0;
//P[1][1] -= K_1 * t_1;
//angle_kalmen += K_0 * angle_err; //最优角度
//q_bias += K_1 * angle_err;
//angle_dot = gyro_m-q_bias;//最优角速度
//return angle_kalmen;
//}
//卡尔曼滤波
//卡尔曼滤波参数与函数
float dt_pitch=0.005;//注意：dt的取值为kalman滤波器采样时间
float angle_kalmen_pitch, angle_dot_pitch;//角度和角速度
float P_pitch[2][2] = {{ 1, 0 },
              { 0, 1 }};
float Pdot_pitch[4] ={ 0,0,0,0};
float Q_angle_pitch=0.001, Q_gyro_pitch=0.005; //角度数据置信度,角速度数据置信度
float R_angle_pitch=0.5 ,C_0_pitch = 1; 
float q_bias_pitch, angle_err_pitch, PCt_0_pitch, PCt_1_pitch, E_pitch, K_0_pitch, K_1_pitch, t_0_pitch, t_1_pitch;

float Kalman_Filter_pitch(float angle_m, float gyro_m)//angleAx 和 gyroGy
{
		angle_kalmen_pitch+=(gyro_m-q_bias_pitch) * dt_pitch;
		angle_err_pitch = angle_m - angle_kalmen_pitch;
		Pdot_pitch[0]=Q_angle_pitch - P_pitch[0][1] - P_pitch[1][0];
		Pdot_pitch[1]=-P_pitch[1][1];
		Pdot_pitch[2]=-P_pitch[1][1];
	
		Pdot_pitch[3]=Q_gyro_pitch;
	
		P_pitch[0][0] += Pdot_pitch[0] * dt_pitch;
		P_pitch[0][1] += Pdot_pitch[1] * dt_pitch;
		P_pitch[1][0] += Pdot_pitch[2] * dt_pitch;
		P_pitch[1][1] += Pdot_pitch[3] * dt_pitch;
	
		PCt_0_pitch = C_0_pitch * P_pitch[0][0];
		PCt_1_pitch = C_0_pitch * P_pitch[1][0];
	
		E_pitch = R_angle_pitch + C_0_pitch * PCt_0_pitch;
	
		K_0_pitch = PCt_0_pitch / E_pitch;
		K_1_pitch = PCt_1_pitch / E_pitch;
		t_0_pitch = PCt_0_pitch;
		
		t_1_pitch = C_0_pitch * P_pitch[0][1];
		
		P_pitch[0][0] -= K_0_pitch * t_0_pitch;
		P_pitch[0][1] -= K_0_pitch * t_1_pitch;
		P_pitch[1][0] -= K_1_pitch * t_0_pitch;
		P_pitch[1][1] -= K_1_pitch * t_1_pitch;
		
		angle_kalmen_pitch += K_0_pitch * angle_err_pitch; //最优角度
		q_bias_pitch += K_1_pitch * angle_err_pitch;
		angle_dot_pitch = gyro_m-q_bias_pitch;//最优角速度
		return angle_kalmen_pitch;
}

//卡尔曼滤波参数与函数
float dt_roll=0.005;//注意：dt的取值为kalman滤波器采样时间
float angle_kalmen_roll, angle_dot_roll;//角度和角速度
float P_roll[2][2] = {{ 1, 0 },
              { 0, 1 }};
float Pdot_roll[4] ={ 0,0,0,0};
float Q_angle_roll=0.001, Q_gyro_roll=0.005; //角度数据置信度,角速度数据置信度
float R_angle_roll=0.5 ,C_0_roll = 1; 

float q_bias_roll, angle_err_roll, PCt_0_roll, PCt_1_roll, E_roll, K_0_roll, K_1_roll, t_0_roll, t_1_roll;
float Kalman_Filter_roll(float angle_m, float gyro_m)//angleAx 和 gyroGy
{
		angle_kalmen_roll += (gyro_m-q_bias_roll) * dt_roll;
		angle_err_roll = angle_m - angle_kalmen_roll;
		Pdot_roll[0]=Q_angle_roll - P_roll[0][1] - P_roll[1][0];
		Pdot_roll[1]=-P_roll[1][1];
		Pdot_roll[2]=-P_roll[1][1];
		Pdot_roll[3]=Q_gyro_roll;
		P_roll[0][0] += Pdot_roll[0] * dt_roll;
		P_roll[0][1] += Pdot_roll[1] * dt_roll;
		P_roll[1][0] += Pdot_roll[2] * dt_roll;
		P_roll[1][1] += Pdot_roll[3] * dt_roll;
		PCt_0_roll = C_0_roll * P_roll[0][0];
		PCt_1_roll = C_0_roll * P_roll[1][0];
		E_roll = R_angle_roll + C_0_roll * PCt_0_roll;
		K_0_roll = PCt_0_roll / E_roll;
		K_1_roll = PCt_1_roll / E_roll;
		t_0_roll = PCt_0_roll;
		t_1_roll = C_0_roll * P_roll[0][1];
		P_roll[0][0] -= K_0_roll * t_0_roll;
		P_roll[0][1] -= K_0_roll * t_1_roll;
		P_roll[1][0] -= K_1_roll * t_0_roll;
		P_roll[1][1] -= K_1_roll * t_1_roll;
		angle_kalmen_roll += K_0_roll * angle_err_roll; //最优角度
		q_bias_roll += K_1_roll * angle_err_roll;
		angle_dot_roll = gyro_m-q_bias_roll;//最优角速度
		return angle_kalmen_roll;
}

//卡尔曼滤波参数与函数
float dt_yaw=0.005;//注意：dt的取值为kalman滤波器采样时间
float angle_kalmen_yaw, angle_dot_yaw;//角度和角速度
float P_yaw[2][2] = {{ 1, 0 },
              { 0, 1 }};
float Pdot_yaw[4] ={ 0,0,0,0};
float Q_angle_yaw=0.001, Q_gyro_yaw=0.005; //角度数据置信度,角速度数据置信度
float R_angle_yaw=0.5 ,C_0_yaw = 1; 
float q_bias_yaw, angle_err_yaw, PCt_0_yaw, PCt_1_yaw, E_yaw, K_0_yaw, K_1_yaw, t_0_yaw, t_1_yaw;
float Kalman_Filter_yaw(float angle_m, float gyro_m)//angleAx 和 gyroGy
{
		angle_kalmen_yaw+=(gyro_m-q_bias_yaw) * dt_yaw;
		angle_err_yaw = angle_m - angle_kalmen_yaw;
		Pdot_yaw[0]=Q_angle_yaw - P_yaw[0][1] - P_yaw[1][0];
		Pdot_yaw[1]=-P_yaw[1][1];
		Pdot_yaw[2]=-P_yaw[1][1];
		Pdot_yaw[3]=Q_gyro_yaw;
		P_yaw[0][0] += Pdot_yaw[0] * dt_yaw;
		P_yaw[0][1] += Pdot_yaw[1] * dt_yaw;
		P_yaw[1][0] += Pdot_yaw[2] * dt_yaw;
		P_yaw[1][1] += Pdot_yaw[3] * dt_yaw;
		PCt_0_yaw = C_0_yaw * P_yaw[0][0];
		PCt_1_yaw = C_0_yaw * P_yaw[1][0];
		E_yaw = R_angle_yaw + C_0_yaw * PCt_0_yaw;
		K_0_yaw = PCt_0_yaw / E_yaw;
		K_1_yaw = PCt_1_yaw / E_yaw;
		t_0_yaw = PCt_0_yaw;
		t_1_yaw = C_0_yaw * P_yaw[0][1];
		P_yaw[0][0] -= K_0_yaw * t_0_yaw;
		P_yaw[0][1] -= K_0_yaw * t_1_yaw;
		P_yaw[1][0] -= K_1_yaw * t_0_yaw;
		P_yaw[1][1] -= K_1_yaw * t_1_yaw;
		angle_kalmen_yaw += K_0_yaw * angle_err_yaw; //最优角度
		q_bias_yaw += K_1_yaw * angle_err_yaw;
		angle_dot_yaw = gyro_m-q_bias_yaw;//最优角速度
		return angle_kalmen_roll;
}


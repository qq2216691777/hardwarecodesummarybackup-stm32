#ifndef _MPU6050_IIC_SIM_H_
#define _MPU6050_IIC_SIM_H_
#include "stm32f10x_conf.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"
#include <math.h>
#include "kalman.h"

/*********************************/
//IO方向设置
#define SDA_IN()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=8<<28;}
#define SDA_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=3<<28;}

//IO操作函数	 
#define IIC_SCL    PBout(6) //SCL
#define IIC_SDA    PBout(7) //SDA	 
#define READ_SDA   PBin(7)  //输入SDA

static void IIC_SIM_GPIO_Init( void );
static void IIC_Sim_Start(void);
static void IIC_Send_Byte(u8 txd);
static u8 IIC_Read_Byte(void);
static u8 IIC_Wait_Ack(void);
void IIC_Send_ACK( u8 Ack);
static void IIC_Stop(void);

void MPU6050_ReadData( float *Angle_Roll, float *Angle_Pitch, float *Angle_Yaw );



/*****************************************/


// 定义MPU6050内部地址
//****************************************
#define	SMPLRT_DIV		0x19	//陀螺仪采样率，典型值：0x07(125Hz)
#define	CONFIG			0x1A	//低通滤波频率，典型值：0x06(5Hz)
#define	GYRO_CONFIG		0x1B	//陀螺仪自检及测量范围，典型值：0x18(不自检，2000deg/s)
#define	ACCEL_CONFIG	0x1C	//加速计自检、测量范围及高通滤波频率，典型值：0x01(不自检，2G，5Hz)
#define	ACCEL_XOUT_H	0x3B
#define	ACCEL_XOUT_L	0x3C
#define	ACCEL_YOUT_H	0x3D
#define	ACCEL_YOUT_L	0x3E
#define	ACCEL_ZOUT_H	0x3F
#define	ACCEL_ZOUT_L	0x40
#define	TEMP_OUT_H		0x41
#define	TEMP_OUT_L		0x42
#define	GYRO_XOUT_H		0x43
#define	GYRO_XOUT_L		0x44	
#define	GYRO_YOUT_H		0x45
#define	GYRO_YOUT_L		0x46
#define	GYRO_ZOUT_H		0x47
#define	GYRO_ZOUT_L		0x48
#define	PWR_MGMT_1		0x6B		//电源管理，典型值：0x00(正常启用)
#define	WHO_AM_I			0x75	//IIC地址寄存器(默认数值0x68，只读)
#define	SlaveAddress	0xD0		//IIC写入时的地址字节数据，+1为读取
//****************************************

void MPU6050_Init( void );
void MPU6050_WriteOneData( u8 RegAddr, u8 W_Data );
u8 MPU6050_ReadOneReg( u8 RegAddr );
short Get_MPU6050_Data( u8 RegAddr );

#endif

#include "mpu6050_iic_sim.h"

void MPU6050_Init( void )
{
	u8 i;
	IIC_SIM_GPIO_Init();

	i = MPU6050_ReadOneReg( WHO_AM_I );
	while( i != 0x68 )
	{
		printf("MPU6055 error !\r\n");
		i = MPU6050_ReadOneReg( WHO_AM_I );
	}
	printf("                      MPU6055 SUCESS! !\r\n");

	MPU6050_WriteOneData( PWR_MGMT_1, 0x00);		//解除休眠状态
	MPU6050_WriteOneData( SMPLRT_DIV, 0x07);
	MPU6050_WriteOneData( CONFIG, 0x06);
	MPU6050_WriteOneData( GYRO_CONFIG, 0x18 );		//陀螺仪量程 +- 2000
	MPU6050_WriteOneData( ACCEL_CONFIG, 0x01 );		//加速度量程 +- 2g
}
void MPU6050_WriteOneData( u8 RegAddr, u8 W_Data )
{
	IIC_Sim_Start();
	IIC_Send_Byte(SlaveAddress);	    //发送写命令
	IIC_Send_Byte(RegAddr);	        //内部寄存器地址
	IIC_Send_Byte(W_Data);	        //内部寄存器数据
	IIC_Stop();						//发送停止信号
}

short Get_MPU6050_Data( u8 RegAddr )
{
	u8 H,L;
	H = MPU6050_ReadOneReg( RegAddr );
	L = MPU6050_ReadOneReg( RegAddr+1 );
	return (H<<8)+L;	 //合成数据
}

u8 MPU6050_ReadOneReg( u8 RegAddr )
{
	u8 Reg_Data;
	IIC_Sim_Start();					//起始信号
	IIC_Send_Byte(SlaveAddress);	    //发送设备地址+写信号

	IIC_Send_Byte(RegAddr);	    		//发送存储单元地址，从0开始
	IIC_Sim_Start();
	IIC_Send_Byte(SlaveAddress+1);	    //发送设备地址+读信号
	Reg_Data = IIC_Read_Byte();			//读出寄存器数据
	IIC_Send_ACK(1);					//发送应答信号
	IIC_Stop();							//发送停止信号
	return Reg_Data;
}

/*****************************读取角度********************************/
void MPU6050_ReadData( float *Angle_Roll, float *Angle_Pitch, float *Angle_Yaw )
{
	float Speed_X,Speed_Y,Speed_Z;				//角速度
	float Angle_X,Angle_Y,Angle_Z;				//角  度
	float Angle_X_Temp;
	float Angle_Y_Temp;
	float Angle_Z_Temp;
	u8 Buf[33];
	u8 i;
	u32 sum;

	Speed_X = ( Get_MPU6050_Data( GYRO_XOUT_H ) + 20 )/16.4;
	Speed_Y = ( Get_MPU6050_Data( GYRO_YOUT_H ) - 3 )/16.4;
	Speed_Z = ( Get_MPU6050_Data( GYRO_ZOUT_H ) + 3 )/16.4;

	Angle_X = Get_MPU6050_Data( ACCEL_XOUT_H ) - 700;		//加速度
	Angle_Y = Get_MPU6050_Data( ACCEL_YOUT_H ) + 260;
	Angle_Z = Get_MPU6050_Data( ACCEL_ZOUT_H ) - 1900;

	Angle_X_Temp = (atan2(Angle_X,sqrt(Angle_Z*Angle_Z+Angle_Y*Angle_Y)))*57.2957;
	Angle_Y_Temp = (atan2(Angle_Y,sqrt(Angle_Z*Angle_Z+Angle_X*Angle_X)))*57.2957;
	
	if( Angle_Z > 0 )						//   180/3.1415926 = 57.2957
	{
			Angle_Z_Temp = (atan2(sqrt(Angle_X*Angle_X+Angle_Y*Angle_Y),Angle_Z))*57.2957;
	}
	else
	{
			Angle_Z_Temp = (atan2(sqrt(Angle_X*Angle_X+Angle_Y*Angle_Y),Angle_Z))*57.2957 - 180;
	}
	
	*Angle_Roll = Kalman_Filter_roll( Angle_X_Temp, Speed_X);
	*Angle_Pitch = Kalman_Filter_pitch( Angle_Y_Temp, Speed_Y);
	*Angle_Yaw = Kalman_Filter_yaw( Angle_Z_Temp, Speed_Z);
	
//  	printf("角速度:\t%f\t%f\t%f \n",Speed_X,Speed_Y,Speed_Z);
//  	printf("重力:\t%f\t%f\t%f\n",Angle_X/16384,Angle_Y/16384,Angle_Z/16384);
	
	Buf[0]=0x88;
	Buf[1]=0xaf;
	Buf[2]=0x1c;
	Buf[3]=(short)(Angle_X_Temp)>>8;			
	Buf[4]=(short)(Angle_X_Temp)%256;//Serial 1原始数据
	Buf[5]=(short)(Angle_Y_Temp)>>8;			
	Buf[6]=(short)(Angle_Y_Temp)%256;//Serial 2
	Buf[7]=(short)(Angle_Z_Temp)>>8;
	Buf[8]=(short)(Angle_Z_Temp)%256;//Serial 3
	Buf[9]=0x0;
	Buf[10]=0x0;
	Buf[11]=0;			//Serial 5	H		
	Buf[12]=0;
	Buf[13]=0;			//Serial 6	H	
	Buf[14]=0x0;
	Buf[15]=0;		//Serial 7	H	
	Buf[16]=0x0;
	Buf[17]=0x0;	//Serial 8	H	
	Buf[18]=0x0;
	Buf[19]=0x0;		//Serial 9	H	
	Buf[20]=0x0;
	Buf[21]=((short)(*Angle_Roll*100))>>8;		//Serial 10	H		
	Buf[22]=((short)(*Angle_Roll*100))%256;
	Buf[23]=((short)(*Angle_Pitch*100))>>8;		//Serial 11	H	
	Buf[24]=((short)(*Angle_Pitch*100))%256;
	Buf[25]=((short)(*Angle_Yaw*100))>>8;
	Buf[26]=((short)(*Angle_Yaw*100))%256;
	Buf[27]=0x0;
	Buf[28]=0x0;
	Buf[29]=0x0;
	Buf[30]=0x0;
		
	sum = 0; 
	for(i=0;i<31;i++)
			sum += Buf[i];
	Buf[31]=sum;
	
	for(i=0;i<32;i++)
	{
		USART_SendData(USART1,Buf[i]);//向串口1发送数据
		 while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	}

	
}




/******************************* ********************/
static void IIC_SIM_GPIO_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );

//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	GPIO_SetBits(GPIOC,GPIO_Pin_13); 

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_SetBits(GPIOB,GPIO_Pin_6 | GPIO_Pin_7); 
	
	IIC_SCL=1;
	IIC_SDA=1;
}

static void IIC_Sim_Start(void)		 //I2C起始信号
{
	SDA_OUT();     //sda线输出
	IIC_SDA=1;
	IIC_SCL=1;	  	  
	
	delay_us(5);
 	IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
	delay_us(5);
	IIC_SCL=0;//钳住I2C总线，准备发送或接收数据 
}

static void IIC_Send_Byte(u8 txd)			//向I2C总线发送一个字节数据
{                        
  u8 t,temp;   
	SDA_OUT(); 	    
    for(t=0;t<8;t++)
    {   
			temp = (txd&0x80)>>7;           
					IIC_SDA=temp;
					txd<<=1; 	  
			delay_us(2);   
			IIC_SCL=1;
			delay_us(5); 
			IIC_SCL=0;	
			delay_us(5);
    }
	IIC_Wait_Ack();	 
}

static u8 IIC_Read_Byte(void)			   //从I2C总线接收一个字节数据
{
	u8 i,receive=0;
	SDA_OUT();
	IIC_SDA = 1;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{     
		IIC_SCL=1;
		delay_us(5);
        receive<<=1;
        if(READ_SDA)receive++;   
		IIC_SCL=0;
		delay_us(5); 
    }					    
    return receive;
}

static u8 IIC_Wait_Ack(void)		   //I2C接收应答信号
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  	   
	IIC_SCL=1;
	delay_us(1);	 
	while(READ_SDA)
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCL=0;//时钟输出0 	   
	return 0;  
}

void IIC_Send_ACK( u8 Ack)		   //I2C发送应答信号
{

	IIC_SDA=Ack;
	delay_us(2);
	IIC_SCL=1;
	delay_us(5);
	IIC_SCL=0;
	delay_us(2);	
}

static void IIC_Stop(void)			//I2C停止信号
{
	SDA_OUT();//sda线输出
	IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
 	delay_us(4);
	IIC_SCL=1;
	delay_us(5); 
	IIC_SDA=1;//发送I2C总线结束信号
	delay_us(5);							   	
}


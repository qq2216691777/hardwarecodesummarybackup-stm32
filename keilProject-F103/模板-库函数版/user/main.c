/* STM32F10X_HD,USE_STDPERIPH_DRIVER */


/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@brief              
  *@HAUST - RobotLab
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"

int main( void )
{
	u32 t;
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	while(1)
	{
	 	printf("串口测试:%d\n",t++);
		delay_ms(300);
		if(t>65536) 
			t=0;
	}
}


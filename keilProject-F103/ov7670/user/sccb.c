#include "sccb.h"

void SCCB_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOE, ENABLE);
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1;			//E1 RESET  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	PEout(1)=1;
}

void StartSCCB(void)
{
		SCCB_SDA_OUT();
    SCCB_SDA=1;     //数据线高电平	   
    SCCB_SCL=1;	    //在时钟线高的时候数据线由高至低
    delay_us(10);  
    SCCB_SDA=0;
    delay_us(10);	 
    SCCB_SCL=0;	    //数据线恢复低电平，单操作函数必要	  
}

//SCCB停止信号
//当时钟为高的时候,数据线的低到高,为SCCB停止信号
//空闲状况下,SDA,SCL均为高电平
void StopSCCB(void)
{
		SCCB_SDA_OUT();
    SCCB_SDA=0;
 		delay_us(10);  
    SCCB_SCL=1;	
    delay_us(10); 
    SCCB_SDA=1;	
    delay_us(10); 
}

//SCCB,写入一个字节 
u8 SCCBwriteByte(u8 dat)
{
	u8 j,res;	
	SCCB_SDA_OUT();	
	for(j=0;j<8;j++) //循环8次发送数据
	{
		if(dat&0x80)SCCB_SDA=1;	
		else SCCB_SDA=0;	
		dat<<=1;
		delay_us(10);
		SCCB_SCL=1;	
		delay_us(10);
		SCCB_SCL=0;	
		delay_us(10);		
	}			 
	SCCB_SDA_IN();		//设置SDA为输入 
	
	SCCB_SCL=1;			//接收第九位,以判断是否发送成功
	delay_us(10);
	if(SCCB_READ_SDA)res=0;  //SDA=1发送失败，返回0
	else res=1;         //SDA=0发送成功，返回1
	SCCB_SCL=0;		 
	SCCB_SDA_OUT();		//设置SDA为输出    
	return res;   
}

//SCCB 读取一个字节
//在SCL的上升沿,数据锁存
u8 SCCBreadByte(void)
{
	u8 temp=0,j;    
	SCCB_SDA_IN();//设置SDA为输入  
	for(j=8;j>0;j--) //循环8次接收数据
	{		     	  
		delay_us(10); 
		SCCB_SCL=1;
		delay_us(10); 
		temp=temp<<1;
		if(SCCB_READ_SDA)temp++;   
		delay_us(10);
		SCCB_SCL=0;
	}	
	SCCB_SDA_OUT();		  //设置SDA为输出    
	return temp;
}
 
/*
-----------------------------------------------
   功能: noAck,用于连续读取中的最后一个结束周期
   参数: 无
 返回值: 无
-----------------------------------------------
*/
void noAck(void)
{
	SCCB_SDA_OUT();
	SCCB_SDA=1;	
	SCCB_SCL=1;	
	delay_us(10); 
	SCCB_SCL=0;	
	delay_us(10); 
	SCCB_SDA=0;	
	delay_us(10); 
}




#include "ov7670.h"
#include "ov7670cfg.h"
//本例程适合OV7670无FIFO 无外部晶振 
// 主控：STM32F103ZET6
// 程序整理 ： 叶念西风
// 日期；2015/6/23
// VCC - 3.3V
// GND - GND
// HREF - A11  
// VSYNC - A10
// XCLK - A8
// PCLK - C9
// SOIC - C13	必须上拉
// SOID - E0  必须上拉
// RESET - E1  高电平
// PWDN - GND
//  只以上引脚可初始化成功
u8 OV7670_Init( void )
{
	u8 temp;
	u16 i;
	OV7670_GPIOx_Init();
	OV7670_GPIOx_CONTRL_CONFIG();
	SCCB_Init();
	XCLK_On();	
	if( 0 == wrOV7670Reg(0x12,0x80))  
		return 1 ; //Reset SCCB
	delay_ms(50); 
	//读取产品型号
 	if( 0==rdOV7670Reg(0x0b, &temp ) )return 1;   
	if(temp !=0x73)return 2;  
	delay_ms(50);
	rdOV7670Reg(0x0a,&temp); 
	if(temp !=0x76)return 2;
	//初始化序列
	for( i=0; i<sizeof(ov7670_init_reg_tbl)/sizeof(ov7670_init_reg_tbl[0]); i++ )
	{
		while(!wrOV7670Reg(ov7670_init_reg_tbl[i][0],ov7670_init_reg_tbl[i][1]));
		delay_ms(5);
	}
	return 0;
}


void OV7670_GPIOx_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin =  0X00FF;						//D0-D7
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void OV7670_GPIOx_CONTRL_CONFIG( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE );
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11|GPIO_Pin_10;   //A11 - href   A10 - vysnc   均为输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;									//PCLK  STM32 IO 口配置为输入 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void XCLK_On( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; 								//XCLK   为OV7670提供时钟  8Mhz
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP ; 
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	RCC_MCOConfig(RCC_MCO_HSE  );//hsi
	
}

void XCLK_Off( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

////////////////////////////
//功能：写OV7670寄存器
//返回：1-成功	0-失败
unsigned char wrOV7670Reg(unsigned char regID, unsigned char regDat)
{
	StartSCCB();
	if(0==SCCBwriteByte(0x42))
	{
		StopSCCB();
		return(0);
	}
	delay_us(10);
  if(0==SCCBwriteByte(regID))
	{
		StopSCCB();
		return(0);
	}
	delay_us(10);
  if(0==SCCBwriteByte(regDat))
	{
		StopSCCB();
		return(0);
	}
  	StopSCCB();
  	return(1);
}
////////////////////////////
//功能：读OV7670寄存器
//返回：1-成功	0-失败
unsigned char rdOV7670Reg(unsigned char regID, unsigned char *regDat)
{
	//通过写操作设置寄存器地址
	StartSCCB();
	if(0==SCCBwriteByte(0x42))
	{
		StopSCCB();
		return(0);
	}
	delay_us(10);
  	if(0==SCCBwriteByte(regID))
	{
		StopSCCB();
		return(0);
	}
	StopSCCB();
	
	delay_us(10);
	
	//设置寄存器地址后，才是读
	StartSCCB();
	if(0==SCCBwriteByte(0x43))
	{
		StopSCCB();
		return(0);
	}
	delay_us(10);
	*regDat=SCCBreadByte();
	noAck();
	StopSCCB();
	return(1);
}

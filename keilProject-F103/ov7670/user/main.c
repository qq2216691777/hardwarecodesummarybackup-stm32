/* STM32F10X_HD,USE_STDPERIPH_DRIVER */
#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "ili9320.h"
#include "ov7670.h"

int main( void )
{
	static unsigned long TimerCnt = 0;
	static unsigned int temp7670 = 0;					 
	u16 value,val,val1,val2;
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Ili9320_Init();
	LCD_Clear( White );
	GUI_Text( 50, 50, "OV7670 Initing.....", 19, Black, White);
	
	while(OV7670_Init());
	GUI_Text( 50, 80, "OV7670 Successed!", 17, Black, White);
	delay_ms(1500);	 
		LCD_Clear( White );
	delay_ms(10);
	ili9320_SetCursor(0,0);
  LCD_WriteReg(0x0050, 0);
  LCD_WriteReg(0x0052, 0);
  LCD_WriteReg(0x0051, 239);
  LCD_WriteReg(0x0053, 319);
	LCD_WriteRAM_Prepare(); // 开显存
	XCLK_On(); // OV7670 XCLK 开
// 	while(1)
// 	{
// 		TimerCnt = 0;
// 		temp7670 = 0;
// 		XCLK_On(); // OV7670 XCLK 开
// 		while( 1==PAin(10) ) ;   // Vsync=H    
// 		while( 0==PAin(10) ) ;   // Vhync=L 		
// 		XCLK_Off();//OV7670 XCLK 关
// 		while(TimerCnt < 76800)
// 		{
// 			XCLK_L; 
//      XCLK_H;
// 			value = GPIOB->IDR;
// 			temp7670 ++;
//             
// 			if( 1==PAin(11) ) // HREF = H ||(LCD_PCLK_STATE)
//       {	
// 			   if((temp7670 == 1))// 高字节||(value & 0x0200) ||(LCD_PCLK_STATE) 
//           {
// 						val1=value& 0x00ff;
// 					}
// 					else // 低字节	 if((temp7670 != 1)||(LCD_PCLK_STATE))  
// 					{
// 						val2= value<<8  ; 	 //
// 						val =ili9320_BGR2RGB(val1 |val2);
// 						temp7670 = 0;
// 						LCD_WriteRAM(val); //TFT GRAM 数据
// 						TimerCnt ++;
//            }
//         } 
// 		
// 				
// 		}
// 		
// 	}
	
	
	while(1)
	{
		TimerCnt = 0;
    temp7670 = 0;
		ili9320_SetCursor(0,0);//设置光标位置 
		LCD_WriteRAM_Prepare();     //开始写入GRAM	
		while( 1==PAin(10) ) ;   // Vsync=H    
    while( 0==PAin(10) ) ;   // Vhync=L 
//		XCLK_Off();   // OV7670 XCLK 开
		while(TimerCnt < 76800)
		{
			temp7670 ++;
			while( 1==PCin(9) ); //PCLK=L
			{
				value = GPIOB->IDR;
				if(temp7670 == 1)// 高字节
                {
                    val = value << 8;
                }
                else // 低字节
                {
                    val |= value;
                    temp7670 = 0;
					          LCD->LCD_RAM=val;				  
                    TimerCnt ++;
                }
			}
			while( PCin(9)==1 );  //PCLK=L
									
		}
	}
}


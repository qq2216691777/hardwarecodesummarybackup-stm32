#ifndef _OV7670_H__
#define _OV7670_H__
#include "stm32f10x.h"
#include "delay.h"
#include "sccb.h"
#include "usart.h"
#define XCLK_H	        GPIOA->BSRR =  GPIO_Pin_8;
#define XCLK_L		    GPIOA->BRR =   GPIO_Pin_8;

u8 OV7670_Init( void );
void OV7670_GPIOx_Init( void );
void OV7670_GPIOx_CONTRL_CONFIG( void );
void XCLK_On( void );
void XCLK_Off( void );
unsigned char rdOV7670Reg(unsigned char regID, unsigned char *regDat);
unsigned char wrOV7670Reg(unsigned char regID, unsigned char regDat);
#endif


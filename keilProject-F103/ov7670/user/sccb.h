#ifndef _SCCB_H__
#define _SCCB_H__

#include "stm32f10x.h"
#include "delay.h"

#define SCCB_SDA_IN()  {GPIOE->CRL&=0XFFFfFFF0;GPIOE->CRL|=0X00000008;}
#define SCCB_SDA_OUT() {GPIOE->CRL&=0XFFFfFFF0;GPIOE->CRL|=0X00000003;}

//IO��������	 
#define SCCB_SCL    PCout(13) //SCL
#define SCCB_SDA    PEout(0) //SDA	 
#define SCCB_READ_SDA    PEin(0)  //����SDA 
#define SCCB_ID 		0X42  			//OV7670��ID

void SCCB_Init( void );
void StartSCCB(void);
void StopSCCB(void);
u8 SCCBwriteByte(u8 dat);
u8 SCCBreadByte(void);
void noAck(void);
#endif


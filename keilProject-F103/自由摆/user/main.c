/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     工程模板   
			@日期     2015—6-23
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "ad.h"
#include "pwm.h"
#include "pid.h"
u16 TARGET_DARA=3955;
long date;
extern float Kp;
extern float Ki;
extern float Kd;
void DIR_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF , ENABLE );	  //使能ADC1通道时钟
 	
	//设置该引脚为复用输出功能,输出TIM3 CH2的PWM脉冲波形	GPIOA.1
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; //TIM2_CH2(A1)  TIM2_CH3(A2)  TIM2_CH4(A3)
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //复用推挽 GPIO_Pin_2 | GPIO_Pin_3|输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOF, &GPIO_InitStructure );//初始化GPIO A	
	GPIO_SetBits( GPIOF,GPIO_Pin_11);
}
int main( void )
{
	long AD_date;
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Adc_Init();
	PWM_Init(5000,36);
	TIM5_Int_Init(1000,719);
	DIR_Init();
	while(1)
	{

//		AD_date = Get_Ad_Date();
//		if( AD_date <1817 )
//		{
//			AD_date += 0;
//		}
//		printf("AD  %ld\n",AD_date);
//		if( AD_date > 4600 || AD_date < 3000 )
//		{
//				TIM_Cmd(TIM3, DISABLE);
//		}
//		else
//		{
//			date = PID_Control( TARGET_DARA, AD_date);
//			if(date<0)
//			{
//				//设置方向
//				date = 0- date;
//				GPIO_ResetBits( GPIOF,GPIO_Pin_11);
//			}
//			else
//			{
//				//设置方向
//					GPIO_SetBits( GPIOF,GPIO_Pin_11);
//			}
//			if( date< 300)
//			{
//				TIM_Cmd(TIM3, DISABLE);
//			}
//			else if( date<2000 )
//			{
//				//printf("AD  %ld\n",date);
//				TIM_Cmd(TIM3, ENABLE);
//				Set_Arr( 2000- date);
//			}		
//		}
		if( USART_RX_STA & 0x8000 )
		{
		
				if( USART_RX_BUF[0] == 'A' )  
				{
					Kp += 0.01;
				}
				if( USART_RX_BUF[0] == 'C' )  
				{
					Ki += 0.01;
				}
				if( USART_RX_BUF[0] == 'E' )  
				{
					Kd += 0.01;
				}
				if( USART_RX_BUF[0] == 'G' )  
				{
					TARGET_DARA += 10;
				}
			

				if( USART_RX_BUF[0] == 'B' )  
				{
					Kp -= 0.01;
				}
				if( USART_RX_BUF[0] == 'D' )  
				{
					Ki -= 0.01;
				}
				if( USART_RX_BUF[0] == 'F' )  
				{
					Kd -= 0.01;
				}
				if( USART_RX_BUF[0] == 'H' )  
				{
					TARGET_DARA -= 10;
				}
				
			printf("P%f  I%f  D%f  T %d\n",Kp,Ki,Kd,TARGET_DARA);
			USART_RX_STA = 0;
		}
		
		
		
	}
}


#ifndef _AD_H__
#define _AD_H__

#include "stm32f10x.h"

void  Adc_Init(void);
u16 Get_Adc(u8 ch);
u16 Get_Ad_Date( void );

#endif



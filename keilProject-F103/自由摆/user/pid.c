#include "pid.h"
#define ERR_MAX 2000
float Kp = 12.5;
float Ki = 1.16;
float Kd = 0.1;
long Err_ek = 0;
long Last_ek;

long PID_Control( long Target, long Now_data )
{
	long ek;
	long uk;
	
	ek = Target-Now_data;
	Err_ek += ek;
	if( Err_ek> ERR_MAX)
	{
		Err_ek = ERR_MAX;
	}
	if( Err_ek< -ERR_MAX)
	{
		Err_ek = -ERR_MAX;
	}
	uk = Kp*ek + Ki*Err_ek + Kd*( ek - Last_ek);
	Last_ek = ek;
	
	return uk;
}


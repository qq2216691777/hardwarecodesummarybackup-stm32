#ifndef _PWM_H__
#define _PWM_H__

#include "stm32f10x.h"

#include "ad.h"
#include "pid.h"

void PWM_Init( u16 arr,u16 psc );
void Set_Arr( u32 tem );
void TIM5_Int_Init(u16 arr,u16 psc);
#endif


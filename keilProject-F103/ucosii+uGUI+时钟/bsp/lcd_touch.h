#ifndef _LCD_TOUCH_H__
#define _LCD_TOUCH_H__

#include "stm32f10x.h"


#define TP_CS()  GPIO_ResetBits(GPIOB,GPIO_Pin_12)
#define TP_DCS() GPIO_SetBits(GPIOB,GPIO_Pin_12)

u16 TPReadX(void);
u16 TPReadY(void);
void Lcd_Touch_Init( void );
u16 TOUCH_X(void);  //�����˲�
u16 TOUCH_Y(void);  //�����˲�
void SpiDelay(unsigned int DelayCnt);
unsigned char SPI_WriteByte(unsigned char data);

#endif


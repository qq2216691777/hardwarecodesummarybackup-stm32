#include "includes.h"

OS_STK TASK_USART_STK[TASK_USART_STK_SIZE];
OS_STK TASK_LCD_STK[TASK_LCD_STK_SIZE];

void Task_Start( void *p_arg )		//开始任务
{
	p_arg = p_arg;
	SysTick_Init();
	
	#if(OS_TASK_STAT_EN > 0)
	OSStatInit();	//	统计任务初始化函数
	#endif
	
	LED_Init();			//提示系统正在运行.....

	OSTaskCreate( Task_Usart, (void *)0,&TASK_USART_STK[TASK_USART_STK_SIZE-1],TASK_USART_PRIO);
	OSTaskCreate( Task_LCD, (void *)0,&TASK_LCD_STK[TASK_LCD_STK_SIZE-1],TASK_LCD_PRIO);
	while(1)
	{
		GPIO_SetBits(GPIOD,GPIO_Pin_13);
		OSTimeDlyHMSM(0,0,0,30);
		GPIO_ResetBits(GPIOD,GPIO_Pin_13);
		OSTimeDlyHMSM(0,0,1,300);
	}
}

/*//任务模板
void Task_( void *p_arg )
{
	p_arg = p_arg;
	
	while(1)
	{

	}
	
}
*/



void Task_Usart( void *p_arg )
{
	p_arg = p_arg;
	uart_init(115200);
	while(1)
	{	
		if( USART_RX_STA&0x8000 )
		{
			printf("SUCCESSED\n");
			USART_RX_STA=0;
		}   
		OSTimeDlyHMSM(0,0,0,10);
	}
	
}

void Task_LCD( void *p_arg )
{
	p_arg = p_arg;
	GUI_Init();
	GUI_SetBkColor( GUI_WHITE );
	GUI_Clear();
	GUI_SetColor( GUI_BLACK );
	GUI_DispString(GUI_GetVersionString());
	while(1)
	{
		OSTimeDlyHMSM(0,0,0,10);
	}


}



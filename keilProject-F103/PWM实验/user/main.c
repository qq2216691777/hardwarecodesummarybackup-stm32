#include <stm32f10x.h>
#include "system_start.h"
#include "uart.h"
#include "nvic.h"
int main( void )
{
	Stm32_Clock_Init( 9 );

	RCC->APB2ENR |= 1<<2;  //使能 A IO
	RCC->APB1ENR |= 1<<1;  //使能 T 3
	GPIOA->CRL &= 0X0FFFFFFF;
	GPIOA->CRL |= 0XB0000000;	   //复用推挽
	GPIOA->ODR |= 1<<7;

	TIM3->PSC = 7199; 			   //预分频器
	TIM3->ARR = 200-1;			   //设置定时器自动重装值	周期20ms
	TIM3->CCR2 = 49;			   //设置占空比
	TIM3->CCMR1 |= 7<<12;		   //CH2  PWM2模式
	TIM3->CCMR1 |= 1<<11;		   //CH2 预装载使能
	TIM3->CCER |= 1<<4;			   //OC2输出使能
	TIM3->CR1 |= 0X0080;		   //APRE使能
	TIM3->CR1 |= 0X01;			   //使能定时器3

	while(1)
	{

	}	
}


#include "nvic.h"


//NVIC_Pre					 :抢占优先级
//NVIC_Sub           :响应优先级
//NVIC_Channel       :中断编号
//NVIC_Group         :中断分组 0~4
void NVIC_Init( u8 NVIC_Pre, u8 NVIC_Sub, u8 NVIC_Channel, u8 NVIC_Group )
{
		u32 temp;
		u8 IPRADDR = NVIC_Channel/4;
		u8 IPROFFSET = NVIC_Channel%4;
		IPROFFSET = IPROFFSET*8+4;
		NVIC_SetGroup( NVIC_Group );
		temp = NVIC_Pre<<(4-NVIC_Group);
		temp |= NVIC_Sub&( 0x0f>>NVIC_Group );
		temp &= 0xf;
		if( NVIC_Channel<32 )
		{
				NVIC->ISER[0] |= 1<<NVIC_Channel;				/* 使能中断位 */
		}
		else
		{
				NVIC->ISER[1] |= 1<<(NVIC_Channel-32);
		}
		NVIC->IP[IPRADDR] |= temp<<IPROFFSET;							/* IP和IPR 设置抢断优先级和响应优先级 */
}

void NVIC_SetGroup( u8 NVIC_Group )
{
		u32 temp, temp1;
		temp1 = ( ~NVIC_Group )& 0x07;
		temp1<<=8;
		temp = SCB->AIRCR;
		temp &= 0x0000f8ff;
		temp |= 0x05fa0000;
		temp |= temp1;
		SCB->AIRCR = temp;	 
}


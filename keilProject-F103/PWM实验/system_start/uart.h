#ifndef _UART_H_
#define _UART_H_

#include <stm32f10x.h>
#include "stdio.h"
#define USART1_RX_LEN 100
void Uart_Init( u32 Baud, char flag );

extern u8 USART1_RX_BUF[USART1_RX_LEN];
extern u16 USART1_RX_STA;

#endif

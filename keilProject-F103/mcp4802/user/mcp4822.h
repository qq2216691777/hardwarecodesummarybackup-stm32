#ifndef _MCP4822__H_
#define _MCP4822__H_

#include "stm32f10x.h"
#include "delay.h"

void Mcp4822_Init( void );
void Mcp4822_Write( u8 reg,u16 date );


#define MCP4822_CS  PAout(1)
#define MCP4822_SCK  PAout(2)
#define MCP4822_SDI  PAout(3)
#define MCP4822_LDAC  PAout(4)


#endif


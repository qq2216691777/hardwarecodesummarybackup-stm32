/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     工程模板   
			@日期     2015—6-23
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "mcp4822.h"

int main( void )
{
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Mcp4822_Init();
  Mcp4822_Write(3,4048);	
	while(1)
	{
		
		delay_ms(10);
	}
}


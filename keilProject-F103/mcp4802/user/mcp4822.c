#include "mcp4822.h"

void Mcp4822_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	 
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE );	//GPIOAʱ��
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; 					//PA.1  CS
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		//�������
	GPIO_Init( GPIOA, &GPIO_InitStructure ); 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; 	//PA.2  SCK
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3; 	//PA.3  SDI
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; 	//PA.4  LDAC
	GPIO_Init( GPIOA, &GPIO_InitStructure );

	MCP4822_CS=1;
	MCP4822_SCK = 0;
	MCP4822_LDAC = 1;
}


void Mcp4822_Write( u8 reg,u16 date )
{
	u8 i;
	u8 j;
	date = date & (reg<<12);
	MCP4822_LDAC = 1;
	MCP4822_CS = 0;
	for(i=0;i<16;i++)
	{
//		for(j=10;j;j--);
		if(date&0X8000)
		{
			MCP4822_SDI = 1;
		}
		else
		{
			MCP4822_SDI = 0;
		}
		date = date<<1;
		for(j=10;j;j--);
		MCP4822_SCK = 1;
		for(j=10;j;j--);
		MCP4822_SCK = 0;
	}
	for(j=10;j;j--);
	MCP4822_CS=1;
	for(j=10;j;j--);
	MCP4822_LDAC = 0;
	for(j=10;j;j--);
	//MCP4822_LDAC = 1;
}

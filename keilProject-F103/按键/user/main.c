/* 按下键时  led灯亮   松开led灯熄灭 */

#include <stm32f10x.h>
#include "system_start.h"

void Key_Scan( void );
void Key_Scan_Init( void );

int main( void )
{
		Stm32_Clock_Init( 9 );
		RCC->APB2ENR |= 0X20;			/* 使能C口 */
		GPIOD->CRH &= 0XFF0FFFFF;
		GPIOD->CRH |= 0X00200000;	
		Key_Scan_Init();
		while( 1 )
		{
				Key_Scan();
		}
}

void Key_Scan_Init( void )
{
		RCC->APB2ENR |= 0X00000010;			/* 使能C口 */
		GPIOC->CRH &= 0XFF0FFFFF;
		GPIOC->CRH |= 0X00800000;				/* C13设置模式 */
}

void Key_Scan( void )
{
		if( !(GPIOC->IDR & 0X2000))
		{
				delay_ms( 10 );
				if( !(GPIOC->IDR & 0X2000) )
				{
						GPIOD->ODR |= 0X2000;
				}
		}
		else
		{
				GPIOD->ODR &= 0Xdfff;
		}
		
	
}


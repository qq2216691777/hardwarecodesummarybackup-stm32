#ifndef _SYSTEM_START_H_
#define _SYSTEM_START_H_

#include <stm32f10x.h>

void SystemInit( void );
void Stm32_Clock_Init( u8 );
void delay_ms( u32 time );

#endif

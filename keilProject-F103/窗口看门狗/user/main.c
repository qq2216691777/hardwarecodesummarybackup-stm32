#include <stm32f10x.h>
#include "system_start.h"
#include "uart.h"
#include "nvic.h"
unsigned char x=0;

int main( void )
{
	x=0;
	Stm32_Clock_Init( 9 );
	
	RCC->APB2ENR |= 1<<5;  //IO D 
	RCC->APB2ENR |= 1<<4;  //IO C 
	GPIOD->CRH &= 0XFF0FFFFF;
	GPIOD->CRH |= 0X00100000; // D 13 LED
	GPIOC->CRH &= 0XFF0FFFFF;
	GPIOC->CRH |= 0X00800000; // C 13 KEY

	GPIOD->ODR &= ~(1<<13);
	delay_ms(500);
	GPIOD->ODR |= 1<<13;

/**********************窗口看门狗***************/
	RCC->APB1ENR |= 1<<11;	  //使能看门狗时钟
	WWDG->CFR |= 3<<7;		  //预分频器
	WWDG->CFR &= 0XF80;
	WWDG->CFR |= 0X5F;		  //设置窗口值
	WWDG->CR |= 0X7F;		  //重装看门口计数器
	WWDG->CR |= 1<<7;		  //启动看门狗

	NVIC_Init( 2, 2, WWDG_IRQn, 2 );
	WWDG->SR = 0X00;	 	  //清除提前中断唤醒标志
	WWDG->CFR |= 1<<9;   	  //使能提前唤醒中断

/**********************窗口看门狗END************/

	while(1)
	{	 
		 if( !(GPIOC->IDR & 1<<13) )
		 {
		
		 }
	}
	
}


/*************窗口看门狗提前唤醒中断服务函数************/

void WWDG_IRQHandler( void )
{
	WWDG->CR = 0X7F;
	WWDG->SR = 0X00;         //清除提前中断唤醒标志位
	if(x)
	{
		GPIOD->ODR |= 1<<13;
	 	x=0;
	}
	else
	{
		GPIOD->ODR = 0;
	    x=1;
	}

}

#ifndef __TIMER_H
#define __TIMER_H
#include "sys.h"

//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//定时器 驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/9/4
//版本：V1.1
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved									  
//********************************************************************************

void Tim2_init(u16 arr,u16 psc);
extern void Moter_gpio_Init(void);
extern void Tim8_init(u16 arr,u16 psc);
void Tim1_init(u16 arr,u16 psc);
extern void Tim4_init(u16 arr,u16 psc);
void Moter_control(u16 MOTO1_1,u16 MOTO1_2,u16 MOTO2_1,u16 MOTO2_2,u16 MOTO3_1,u16 MOTO3_2,u16 MOTO4_1,u16 MOTO4_2,u16 MOTO5_1,u16 MOTO5_2,u16 MOTO6_1,u16 MOTO6_2);
#endif

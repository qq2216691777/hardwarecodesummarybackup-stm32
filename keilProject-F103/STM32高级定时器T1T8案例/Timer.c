#include "Timer.h"
#include "usart.h"
#include "stm32f10x_tim.h"
//////////////////////////////////////////////////////////////////////////////////	 
#define Moto_PwmMax 7199
void TIM3_Int_Init(u16 arr,u16 psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能

	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	 计数到5000为500ms
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值  10Khz的计数频率  
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断

	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

	TIM_Cmd(TIM3, ENABLE);  //使能TIMx外设
							 
}
//定时器3中断服务程序
void TIM3_IRQHandler(void)   //TIM3中断
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
		{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 

		}
}


void Tim4_init(u16 arr,u16 psc)
{
	TIM_TimeBaseInitTypeDef		TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  				TIM_OCInitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);			//使能定时器2
	/* Compute the prescaler value */
	//PrescalerValue = (uint16_t) (SystemCoreClock / 24000000) - 1;
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = arr;		//计数上线	
	TIM_TimeBaseStructure.TIM_Prescaler = psc;	//pwm时钟分频
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;		//向上计数
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	/* PWM1 Mode configuration: Channel */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;		//选择定时器模式:TIM脉冲宽度调制模式1
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;			//比较输出使能
	TIM_OCInitStructure.TIM_Pulse = 0;//初始占空比为0
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;       //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);														//根据T指定的参数初始化外设TIM3 OC2
	TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
	TIM_OC2Init(TIM4, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
	TIM_OC3Init(TIM4, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
	TIM_OC4Init(TIM4, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM4, ENABLE);
	TIM_Cmd(TIM4, ENABLE);
}

void Tim8_init(u16 arr,u16 psc)
{
// 	RCC->APB2ENR|=1<<13; //TIM8时钟使能         


// //////////////////////////////////////// 
// //  GPIOA->CRH&=0XFFFF0FF0;//PA8 11 输出
// //  GPIOA->CRH|=0X0000B00B;//复用功能输出    
//  
// ////////////////////////////////////////
//  TIM8->BDTR |=0xC0;   //ARPE使能 
//  TIM8->ARR=arr;//设定计数器自动重装值 
//  TIM8->PSC=psc;//预分频器不分频
//  
//  TIM8->CCMR1|=6<<4;  //CH2 PWM2模式 CH1/2  
//  TIM8->CCMR1|=1<<3; //CH2预装载使能

//  TIM8->CCMR2|=6<<12;  //CH2 PWM2模式 CH3/4   
//  TIM8->CCMR2|=1<<11; //CH2预装载使能

//  

//  TIM8->CCER|=1<<0;   //OC2 输出使能
//  TIM8->CCER|=1<<12;   //OC2 输出使能


//  TIM8->CR1 |=0x80;   //ARPE使能 
//  TIM8->CR1|=0x01;    //使能定时器1

//  TIM8->BDTR|=0x8000;  //使能定时器1输出 
//  

 RCC->APB2ENR|=1<<13;       //TIM8时钟使能  
     
  TIM8->ARR=arr;  //设定计数器自动重装值//刚好1ms    
 TIM8->PSC=psc;  //预分频器7200,得到10Khz的计数时钟
 //这两个东东要同时设置才可以使用中断
//  TIM8->DIER|=1<<0;   //允许更新中断    
//  TIM8->DIER|=1<<6;   //允许触发中断
//                
//  TIM8->CR1|=0x01;    //使能定时器8   
//MY_NVIC_Init(1,3,TIM1_CC_IRQChannel,2);//抢占1，子优先级3，组2           
//PWM输出初始化
//arr：自动重装值
//psc：时钟预分频数
       
 //此部分需手动修改IO口设置     
 RCC->APB2ENR|=1<<13;   //TIM8时钟使能     
 //RCC->APB2ENR|=1<<4;   //使能PORTC口时钟  
//   GPIOC->CRL&=0XF0FFFFFF;//PC6输出 
//  GPIOC->CRL|=0X0B000000;//复用功能输出     
//  GPIOC->ODR|=1<<6;//PC6上拉  
//  GPIOC->CRL&=0X0FFFFFFF;//PC7输出 
//  GPIOC->CRL|=0XB0000000;//复用功能输出     
//  GPIOC->ODR|=1<<7;//PC7上拉  
//  GPIOC->CRH&=0XFFFFFFF0;//PC8输出 
//  GPIOC->CRH|=0X0000000B;//复用功能输出     
//  GPIOC->ODR|=1<<8;//PC8上拉  
//  GPIOC->CRH&=0XFFFFFF0F;//PC9输出 
//  GPIOC->CRH|=0X000000B0;//复用功能输出     
//  GPIOC->ODR|=1<<9;//PC9上拉    
 TIM8->ARR=arr;//设定计数器自动重装值  
 TIM8->PSC=psc;//预分频器不分频 
//  TIM8->CCMR1|=7<<12;  //CH2 PWM2模式   
//  TIM8->CCMR1|=1<<11;  //CH2预装载使能
TIM8->CCMR1|=0X6464;  //CH2 PWM1模式,CH2预装载使能,CH1 PWM1模式,CH1预装载使能
 TIM8->CCMR2|=0X6464; 

// TIM1->CCMR2|=7<<20;  //CH2 PWM2模式 CH3/4   
//  TIM1->CCMR2|=1<<19; //CH2预装载使能      
// TIM8->CCER|=1<<4;   //OC2 输出使能 
 

TIM8->CCER|=0X1111;   //OC1-4 输出使能
 
 
 TIM8->BDTR=0X8000;  //设置PMW主输出 
 TIM8->CR1|=0x01;    //使能定时器8  




		  
}
void Tim1_init(u16 arr,u16 psc)
{
	RCC->APB2ENR|=1<<11;       //TIM1时钟使能   


//////////////////////////////////////// 
 GPIOA->CRH&=0XFFFF0FF0;//PA8 11 输出
 GPIOA->CRH|=0X0000B00B;//复用功能输出    
 
////////////////////////////////////////
 TIM1->BDTR |=0xC0;   //ARPE使能 
 TIM1->ARR=arr;//设定计数器自动重装值 
 TIM1->PSC=psc;//预分频器不分频
 
 TIM1->CCMR1|=6<<4;  //CH2 PWM2模式 CH1/2  
 TIM1->CCMR1|=1<<3; //CH2预装载使能

 TIM1->CCMR2|=6<<12;  //CH2 PWM2模式 CH3/4   
 TIM1->CCMR2|=1<<11; //CH2预装载使能

 

 TIM1->CCER|=1<<0;   //OC2 输出使能
 TIM1->CCER|=1<<12;   //OC2 输出使能


 TIM1->CR1 |=0x80;   //ARPE使能 
 TIM1->CR1|=0x01;    //使能定时器1

 TIM1->BDTR|=0x8000;  //使能定时器1输出 

	
	 
 
}


	void Moter_gpio_Init(void)
{
	
	GPIO_InitTypeDef GPIO_InitStructure;			//使能电机用的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOB, ENABLE); //设置电机使用到得管脚
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA , ENABLE); //设置电机使用到得管脚
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 |GPIO_Pin_7 | GPIO_Pin_8| GPIO_Pin_9; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 					//复用推挽输出
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6| GPIO_Pin_7| GPIO_Pin_8| GPIO_Pin_9 ; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 					//复用推挽输出
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8 |GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 ; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 					//复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}
	void Moter_control(u16 MOTO1_1,u16 MOTO1_2,u16 MOTO2_1,u16 MOTO2_2,u16 MOTO3_1,u16 MOTO3_2,u16 MOTO4_1,u16 MOTO4_2,u16 MOTO5_1,u16 MOTO5_2,u16 MOTO6_1,u16 MOTO6_2)
{
		
	if(MOTO1_1>Moto_PwmMax)	MOTO1_1 = Moto_PwmMax;
	if(MOTO1_2>Moto_PwmMax)	MOTO2_2 = Moto_PwmMax;
	if(MOTO2_1>Moto_PwmMax)	MOTO2_1 = Moto_PwmMax;
	if(MOTO2_2>Moto_PwmMax)	MOTO2_2 = Moto_PwmMax;
	if(MOTO3_1>Moto_PwmMax)	MOTO3_1 = Moto_PwmMax;
	if(MOTO3_2>Moto_PwmMax)	MOTO3_2 = Moto_PwmMax;
	if(MOTO4_1>Moto_PwmMax)	MOTO4_1 = Moto_PwmMax;
	if(MOTO4_2>Moto_PwmMax)	MOTO4_2 = Moto_PwmMax;
	if(MOTO5_1>Moto_PwmMax)	MOTO5_1 = Moto_PwmMax;
	if(MOTO5_2>Moto_PwmMax)	MOTO5_2 = Moto_PwmMax;
	if(MOTO6_1>Moto_PwmMax)	MOTO6_1 = Moto_PwmMax;
	if(MOTO6_2>Moto_PwmMax)	MOTO6_2 = Moto_PwmMax;
	
	
// 	if(MOTO1_1<0)	MOTO1_1 = 0;
// 	if(MOTO1_2<0)	MOTO2_2 = 0;
// 	if(MOTO2_1<0)	MOTO2_1 = 0;
// 	if(MOTO2_2<0)	MOTO2_2 = 0;
// 	if(MOTO3_1<0)	MOTO3_1 = 0;
// 	if(MOTO3_2<0)	MOTO3_2 = 0;
// 	if(MOTO4_1<0)	MOTO4_1 = 0;
// 	if(MOTO4_2<0)	MOTO4_2 = 0;
	
	TIM4->CCR1 = MOTO1_1;
	TIM4->CCR2 = MOTO1_2;
	TIM4->CCR3 = MOTO2_1;
	TIM4->CCR4 = MOTO2_2;
	
	TIM1->CCR1 = MOTO3_1;
	TIM1->CCR2 = MOTO3_2;
	TIM1->CCR3 = MOTO3_1;
	TIM1->CCR4 = MOTO3_2;
	
	TIM8->CCR1 = MOTO5_1;
	TIM8->CCR2 = MOTO5_2;
	TIM8->CCR3 = MOTO6_1;
	TIM8->CCR4 = MOTO6_2;

}



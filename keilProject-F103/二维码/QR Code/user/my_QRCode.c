#include "my_qrcode.h"
/*
函数名：  void DISPLAY_RENCODE_TO_TFT(u8 *qrcode_data, u16 t_x, u16 t_y, float big )
参数：	qrcode_data  二维码的内容
		t_x         显示的屏幕坐标 起始坐标
		t_y         显示的屏幕起始坐标
		big         图像大小
*/
void DISPLAY_RENCODE_TO_TFT(u8 *qrcode_data, u16 t_x, u16 t_y, float big )
{
	u8 i,j;
	u16 x,y,p;
	
	EncodeData((char *)qrcode_data);
	if(m_nSymbleSize*2>240)	
	{
		//LCD_ShowString(10,60,200,16,16,(u8 *)"The QR Code is too large!");//太大显示不下
		return;
	}
	for(i=0;i<10;i++)
	{
		if((m_nSymbleSize*i*2)>240)	break;
	}
	p=(i-1)*big;		//点大小 1.5控制大小
	x=t_x;				//(240-m_nSymbleSize*p)/2;
	y=t_y;
	for(i=0;i<m_nSymbleSize;i++)
	{
		for(j=0;j<m_nSymbleSize;j++)
		{
			if(m_byModuleData[i][j]==1)
				LCD_Clear(x+p*i,y+p*j,x+p*(i+1),y+p*(j+1),Black);

		}
			
	}
}


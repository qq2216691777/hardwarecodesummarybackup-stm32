#ifndef _FATFS_H__
#define _FATFS_H__

#include "sd_sdio.h"
#include "ff.h"
#include "LinkList.h"
#define FIFO_CHAR_NUM 60

FRESULT Fatfs_Init( void );
void Data_Init( void );
u8 Check_Form( char* c_data );
void Char_To_Num( char* dataa, LinkListNode* datater );
void Char_To_9_Num( char* dataa, LinkListNode* datater );
long long Char_To_Number( char* str );
LinkListNode* Find_Position( long long num );
unsigned char exf_getfree(unsigned char *drv,unsigned long *total,unsigned long *free);

#endif

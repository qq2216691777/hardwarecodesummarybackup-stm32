#include "fatfs.h"
#include "usart.h"
#include "ff.h"
#include "ili9320.h"
#include <stdlib.h>


DWORD totle;
FATFS *fs11;

FATFS fs;

DIR DirInf;
FIL fdst;
FIL fdst_err;
FRESULT res;
const BYTE textFileBuffer[] = "全国大学生机械创新大赛\r\n\r\n请将数据库文件命名为ISS_data.txt并保存在/ISS目录下\r\n\r\n叶念西风\r\n机器人创新实验室\r\n";
UINT bw;

char dataa[FIFO_CHAR_NUM+4];
TLinkList* list;

FRESULT Fatfs_Init( void )
{
	if( f_mount( 0, &fs ) != FR_OK )
	{
		while(1);
	}
	
	return f_getfree( "0", &totle, &fs11);

}

void Data_Init( void )
{
	u8 i=0;
	u32 sd_free;
	u32 sd_total;
	
	GUI_Text(30, 50,"SD FATFS Initing...", White, Red );
	
	if( Fatfs_Init() != FR_OK )
	{
		GUI_Text(30, 70,"SD FATFS Failed", White, Red );
		GUI_Text(30, 90,"Please Check The SD", White, Red );
		while(1);
	}
	else
		GUI_Text(30, 70,"SD FATFS IS OK", White, Red );
	
	if( f_opendir(&DirInf, "/ISS") != FR_OK )
	{
		if(f_mkdir( "ISS" ) == FR_OK )
		{
			
			if( f_open( &fdst_err,"0:/ISS/README.txt",FA_CREATE_ALWAYS|FA_WRITE ) == FR_OK )
			{
				f_write( &fdst_err, textFileBuffer, sizeof(textFileBuffer), &bw );
				
				f_close( &fdst_err );
				
				GUI_Text(30, 90,"Can't Find The Dir", White, Red );
				GUI_Text(30, 110,"Please Read The README.txt", White, Red );
				while(1);
			}
		}	
	}
	else if(  f_open( &fdst,"0:/ISS/ISS_data.txt",FA_READ ) == FR_NO_FILE )
	{
		f_close( &fdst );
		if( f_open( &fdst_err,"0:/ISS/README.txt",FA_CREATE_ALWAYS|FA_WRITE ) == FR_OK )
		{
			f_write( &fdst_err, textFileBuffer, sizeof(textFileBuffer), &bw );
			
			f_close( &fdst_err );
			
			GUI_Text(30, 90,"Can't Find The Data File", White, Red );
			GUI_Text(30, 110,"Please Read The README.txt", White, Red );
			while(1);
		}
	}
	else
	{
		f_close( &fdst );
		GUI_Text(30, 90,"The Data File Is OK", White, Red );
	}
	
	
	
	f_open( &fdst,"0:/ISS/ISS_data.txt",FA_READ );
	f_gets( dataa, FIFO_CHAR_NUM, &fdst );
	if(Check_Form( dataa ))
	{
		while( dataa[i++]<127 )
		{
			if(i>10)
			{
				GUI_Text(30, 130,"Please Check The Data Form", White, Red );
				while(1);
			}
		}
		if( dataa[i]<127 || dataa[i+1]<127 || dataa[i+3]>127)
		{
			GUI_Text(30, 130,"Please Check The Data Form", White, Red );
			while(1);
		}
		else
		{
			GUI_Text(30, 110,"The Data Form Is OK", White, Red );
			GUI_Text(30, 130,"Reading The Data File...", White, Red );
		}
	}
	else
	{
		GUI_Text(30, 130,"Please Check The Data Form", White, Red );
		while(1);
	}
	
	i=0;
	f_gets( dataa, FIFO_CHAR_NUM, &fdst );
	while( Check_Form( dataa ) &&  dataa[0]!=0x30 )
	{
		i++;
		if( f_eof(&fdst) )
			break;
		
		f_gets( dataa, FIFO_CHAR_NUM, &fdst );
		
	}
	f_close( &fdst );
	f_open( &fdst,"0:/ISS/ISS_data.txt",FA_READ );
	f_gets( dataa, FIFO_CHAR_NUM, &fdst );
	
	list = LinkList_Create();
	list->length = i;
	
	
	f_gets( dataa, FIFO_CHAR_NUM, &fdst );	
	Char_To_9_Num( dataa, &(list->header) );
	
	GUI_Text(30, 150,"Data Read Is OK ", White, Red );
	GUI_Text(30, 170,"Total Number: ", White, Red );
	GUI_Text_Num( 130, 170, list->length, White, Red);
	
	f_close( &fdst );
	
	exf_getfree( "0", &sd_total, &sd_free );
	
	
	GUI_Text(30, 190,"Total:      KB", White, Red );
	GUI_Text_Num( 80, 190, sd_total>>10, White, Red);
	
	GUI_Text(190, 190,"Free:      KB", White, Red );
	GUI_Text_Num( 230, 190, sd_free>>10, White, Red);
	
}


/*
	函数名：u8 Check_Form( char* c_data )
	参数：  待检查的数据格式
	返回值：1 true  0 fause
	描述： 判断读取的一段的数据中换行符的位置
*/
u8 Check_Form( char *c_data )
{
	u8 i=0;
	while( c_data[i] != 0x0d )
	{
		if(i++>FIFO_CHAR_NUM)
		{
			return 0;
		}
	}
	if(i<8)
		return 0;
	else
		return 1;
}
/*
读取最多9个数据
*/
void Char_To_Num( char* dataa, LinkListNode* datater )
{
	u8 i=0;
	u8 leng;
	u16 first_n=0;
	long long second_n=0;
	u8 third_n=0;
	u8 fort_n=0;
	
	leng=0;
	while( 1 )
	{
		i=0;
		while( dataa[i]<'0'||dataa[i]>'9') /*  判断编号 */
		{
			i++;
		}
		while( dataa[i]>='0' && dataa[i]<='9' )
		{
			first_n = first_n*10;
			first_n +=dataa[i]-0x30;
			i++;
		}
		if(first_n > list->length +30 )
		{
			GUI_Text(30, 130,"Please Check The Data Form", White, Red );
			while(1);
		}
		
		
		while( dataa[i]<'0'||dataa[i]>'9')	/* 判断快递编号*/
		{
			i++;
		}
		
		while( dataa[i]>='0' && dataa[i]<='9' )
		{
			second_n = second_n*10;
			second_n +=dataa[i]-0x30;
			i++;
		}
		
		while( dataa[i]<127 )	/* 判断发件人地址  未编辑*/
		{
			i++;
		}
		
		while( dataa[i]>126 && dataa[i+2]>126 )
		{
			third_n = third_n*10;
			third_n +=dataa[i]-0x30;
			i++;
		}
		
		while( dataa[i]<127 )	/* 判断收件人地址  未编辑*/
		{
			i++;
		}
		
		while( dataa[i]>126 && dataa[i+2]>126 )
		{
			fort_n = fort_n*10;
			fort_n +=dataa[i]-0x30;
			i++;
		}
		
		datater->Num = first_n;
		datater->Order_Num = second_n;
		first_n=0;
		second_n=0;
		
		leng ++;
		
		if( f_eof(&fdst) )
		{
			break;
		}
		
		f_gets( dataa, FIFO_CHAR_NUM, &fdst );
		
		if( Check_Form( dataa ) && leng<9 )
		{
			
			datater = datater->next ;
			
		}	
		else
		{
			break;
		}
	}	 
}

/*
读取最多9个数据
*/
void Char_To_9_Num( char* dataa, LinkListNode* datater )
{
	u8 i=0;
	u8 leng;
	u16 first_n=0;
	long long second_n=0;
	u8 third_n=0;
	u8 fort_n=0;
	
	leng=0;
	while( 1 )
	{
		i=0;
		while( dataa[i]<'0'||dataa[i]>'9') /*  判断编号 */
		{
			i++;
		}
		while( dataa[i]>='0' && dataa[i]<='9' )
		{
			first_n = first_n*10;
			first_n +=dataa[i]-0x30;
			i++;
		}
		if(first_n > list->length +30 )
		{
			GUI_Text(30, 130,"Please Check The Data Form", White, Red );
			while(1);
		}
		
		
		while( dataa[i]<'0'||dataa[i]>'9')	/* 判断快递编号*/
		{
			i++;
		}
		
		while( dataa[i]>='0' && dataa[i]<='9' )
		{
			second_n = second_n*10;
			second_n +=dataa[i]-0x30;
			i++;
		}
		
		while( dataa[i]<127 )	/* 判断发件人地址  未编辑*/
		{
			i++;
		}
		
		while( dataa[i]>126 && dataa[i+2]>126 )
		{
			third_n = third_n*10;
			third_n +=dataa[i]-0x30;
			i++;
		}
		
		while( dataa[i]<127 )	/* 判断收件人地址  未编辑*/
		{
			i++;
		}
		
		while( dataa[i]>126 && dataa[i+2]>126 )
		{
			fort_n = fort_n*10;
			fort_n +=dataa[i]-0x30;
			i++;
		}
		
		datater->Num = first_n;
		datater->Order_Num = second_n;
		first_n=0;
		second_n=0;
		
		leng ++;
		
		if( f_eof(&fdst) )
		{
			break;
		}
		
		f_gets( dataa, FIFO_CHAR_NUM, &fdst );
		
		if( Check_Form( dataa ) && leng<9 )
		{
			
			datater->next = (LinkListNode*)malloc(sizeof(LinkListNode));
			if( datater->next!=NULL)
			{
				datater = datater->next;
			}
			else
			{
				GUI_Text(30, 150,"malloc error   ", White, Red );
				while(1);
			}	
		}	
		else
		{
			break;
		}
	}	 
}

long long Char_To_Number( char* str )
{
	long long number=0;
	u8 i=0;
	while( str[i]>='0' && str[i]<='9' )
	{
		number = number*10;
		number +=str[i]-0x30;
		i++;
	}
	
	return number;
}

LinkListNode* Find_Position( long long num )
{
	LinkListNode* p;
	
	p=&(list->header);
	
	while( p->Order_Num != num)
	{
		if(p->next != NULL)
		{
			p=p->next;
		}
		else
		{
			return NULL;
		}
		
	}
	return p;
}

//得到磁盘剩余容量
//drv:磁盘编号("0:"/"1:")
//total:总容量	 （单位KB）
//free:剩余容量	 （单位KB）
//返回值:0,正常.其他,错误代码
unsigned char exf_getfree(unsigned char *drv,unsigned long *total,unsigned long *free)
{
	FATFS *fs1;
	u8 res;
    DWORD fre_clust=0, fre_sect=0, tot_sect=0;
    //得到磁盘信息及空闲簇数量
    res = f_getfree((const TCHAR*)drv, &fre_clust, &fs1);
    if(res==0)
	{											   
	    tot_sect=(fs1->n_fatent-2)*fs1->csize;	//得到总扇区数
	    fre_sect=fre_clust*fs1->csize;			//得到空闲扇区数	   
		*total=tot_sect>>1;	//单位为KB
		*free=fre_sect>>1;	//单位为KB 
 	}
	return res;
}



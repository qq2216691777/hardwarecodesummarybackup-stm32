/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ******************************************************************************
  *@file      main.c
  *@author    叶念西风
  *@date 	  2016.1.2
  *@version      
  *@brief     QR_code         
  *@HAUST - RobotLab           
  ******************************************************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "ili9320.h"
#include "timer.h"
#include "qr_encode.h"
#include "my_qrcode.h"


void LED_Init(void);






const u8 codetest[]={
	"河南科技大学机器人创新实验室-ynxf"
};

int main( void )
{
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	LED_Init();
	TIM3_Int_Init( 4999, 7199 );
	
	Ili9320_Init();
	LCD_Clear( 0, 0, 320, 240, White );
	DISPLAY_RENCODE_TO_TFT((u8 *)codetest,10,10,0.8 );
	while(1);
	
	

}

void LED_Init(void)
{
 
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);	 //使能PB,PE端口时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;				 //LED0-->PB.5 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(GPIOD, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5
 
}


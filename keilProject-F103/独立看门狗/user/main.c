#include <stm32f10x.h>
#include "system_start.h"
#include "uart.h"
#include "nvic.h"

int main( void )
{
	Stm32_Clock_Init( 9 );
	
	RCC->APB2ENR |= 1<<5;  //IO D 
	RCC->APB2ENR |= 1<<4;  //IO C 
	GPIOD->CRH &= 0XFF0FFFFF;
	GPIOD->CRH |= 0X00100000; // D 13 LED
	GPIOC->CRH &= 0XFF0FFFFF;
	GPIOC->CRH |= 0X00800000; // C 13 KEY
//	GPIOC->ODR |= 1<<13;

	GPIOD->ODR &= ~(1<<13);
	delay_ms(300);
	GPIOD->ODR |= 1<<13;

/**********************看门狗***************/
	IWDG->KR = 0X5555;
	IWDG->PR = 3;
	IWDG->RLR = 625;	//喂狗时间为1000ms
	IWDG->KR = 0XAAAA;
	IWDG->KR = 0XCCCC;  //启动独立看门狗  一旦开启无法关闭
/**********************看门狗END************/

	while(1)
	{	 
		 if( !(GPIOC->IDR & 1<<13) )
		 {
		    IWDG->KR = 0XAAAA; 
		 }
	}
	
}

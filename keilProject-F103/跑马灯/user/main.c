#include <stm32f10x.h>
#include "system_start.h"

int main( void )
{
		Stm32_Clock_Init( 9 );
		RCC->APB2ENR |= 1<<5;		/* 	使能d口时钟 */
		RCC->APB2ENR |= 1<<8;		/* 	使能g口时钟 */
		GPIOD->CRH &= 0XFF0FFFFF;
		GPIOD->CRH |= 0X00200000;  /* 设置d13模式 */
		GPIOG->CRH &= 0XF0FFFFFF;
		GPIOG->CRH |= 0X02000000;  /* 设置G14模式 */	
	
		while( 1 )
		{
				GPIOD->ODR |= 0x2000;
				GPIOG->ODR &= 0XBFFF;
				delay_ms( 400 );
				GPIOD->ODR &= 0XDFFF;
				GPIOG->ODR |= 0X4000;
				delay_ms( 400 );
			
		}	
}


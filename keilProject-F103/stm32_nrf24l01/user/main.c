#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "nrf24l01_spi.h"

u8 NRF_Status;
u8 NRF_Rxbuf[RX_PLOAD_WIDTH];
u8 NRF_Txbuf[TX_PLOAD_WIDTH];
u8 NRF_Data_num;
int main( void )
{
	delay_init();
	NVIC_Configuration();
	uart_init( 9600 );
	NRF24l01_SPI_Init();	//SPI2
	NRF_Status = NRF_Check();
	if( NRF_Status )
	{
		printf("\r\nNRF与MCU连接成功！\r\n");
	}
	else
	{
		printf("\r\n  NRF与MCU连接失败，请重新检查接线。\r\n");
	}
	
	while(1)
	{
		NRF_RX_Mode();
		NRF_Status = NRF_Rx_Dat( NRF_Rxbuf );
		 switch( NRF_Status )
	    {
	      case RX_DR:
	      for( NRF_Data_num=0; NRF_Data_num<RX_PLOAD_WIDTH; NRF_Data_num++ )
	      {					
	          printf("\r\n 主机端 接收到 从机端 发送的数据为：%d \r\n", NRF_Rxbuf[NRF_Data_num] );
	          NRF_Txbuf[ NRF_Data_num ] = NRF_Rxbuf[ NRF_Data_num ];
	      }
	      break;
	
	      case ERROR:
	        	printf("\r\n 主机端 接收出错。   \r\n");
	      break;  		
	    }

	}
}




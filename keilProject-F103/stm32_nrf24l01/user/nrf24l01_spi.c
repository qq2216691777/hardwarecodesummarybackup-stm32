#include "nrf24l01_spi.h"


 u8 RX_BUF[ RX_PLOAD_WIDTH ];		//接收数据缓存
 u8 TX_BUF[ TX_PLOAD_WIDTH ];		//发射数据缓存
 u8 TX_ADDRESS[ TX_ADR_WIDTH ] = {0x01, 0x01, 0x01, 0x01, 0x02};  // 定义一个静态发送地址
 u8 RX_ADDRESS[ RX_ADR_WIDTH ] = {0x01, 0x01, 0x01, 0x01, 0x01};

void NRF24l01_SPI_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE );
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );				   			//初始化gpiob
	GPIO_SetBits( GPIOB, GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 );	    //上拉

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;		  		//CE
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOC, &GPIO_InitStructure );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	GPIO_SetBits( GPIOB, GPIO_Pin_2 );			     		//CSN置高

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;		 		//IRQ
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;  		//上拉输入
	GPIO_Init( GPIOB, &GPIO_InitStructure ); 


	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;	 //设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;						 //设置SPI工作模式:设置为主SPI
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;					 //设置SPI的数据大小:SPI发送接收8位帧结构
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;							 //串行同步时钟的空闲状态为低电平
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;						 //串行同步时钟的第一个跳变沿（上升或下降）数据被采样
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;							 //NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;   //定义波特率预分频的值:波特率预分频值为8
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;					 //指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
	SPI_InitStructure.SPI_CRCPolynomial = 7;							 //CRC值计算的多项式
	SPI_Init( SPI2, &SPI_InitStructure );								 //根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器
	SPI_Cmd( SPI2, ENABLE );
	SPI2_ReadWriteByte( 0xff );											 //启动传输
}

u8 SPI2_ReadWriteByte( u8 TxData )
{		
	u8 retry=0;				 	
	while (SPI_I2S_GetFlagStatus( SPI2, SPI_I2S_FLAG_TXE) == RESET ) //检查指定的SPI标志位设置与否:发送缓存空标志位
		{
			retry ++;
			if( retry>200 )return 0;
		}			  
	SPI_I2S_SendData( SPI2, TxData ); //通过外设SPIx发送一个数据
	retry = 0;

	while ( SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET ) //检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
			retry++;
			if(retry>200)return 0;
		}	  						    
	return SPI_I2S_ReceiveData( SPI2 ); //返回通过SPIx最近接收的数据					    
}

u8 NRF_Check( void )
{
	u8 buf[5]={ 0xc2,0xc2,0xc2,0xc2,0xc2 };
	u8 buf1[5];
	u8 i;	 
	SPI_NRF_WriteBuf( NRF_WRITE_REG+TX_ADDR, buf, 5 );	 /*写入5个字节的地址.  */ 	
	SPI_NRF_ReadBuf( TX_ADDR,buf1, 5);					 /*读出写入的地址 */
   	for( i=0; i<5; i++ )								 /*比较*/  
	{
		if( buf1[i] != 0xC2 )
		break;
	}
	if( i==5 )
		return 1 ;        //MCU与NRF成功连接 
	else
		return 0 ;        //MCU与NRF不正常连接  
}

u8 SPI_NRF_RW( u8 dat )
{  	  
  while ( SPI_I2S_GetFlagStatus( SPI2, SPI_I2S_FLAG_TXE ) == RESET ); /* 当 SPI发送缓冲器非空时等待 */   
  SPI_I2S_SendData( SPI2, dat );									  /* 通过 SPI2发送一字节数据 */ 
  while ( SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET );  /* 当SPI接收缓冲器为空时等待 */
  return SPI_I2S_ReceiveData( SPI2 );								   /* Return the byte read from the SPI bus */
}

u8 SPI_NRF_WriteBuf( u8 reg, u8 *pBuf, u8 bytes)
{
	u8 status,byte_cnt;
	NRF_CE = 0;										//CE置底
	NRF_CSN = 0;									//CSN置底
	status = SPI_NRF_RW( reg );						/*发送寄存器号*/
	for( byte_cnt=0; byte_cnt<bytes; byte_cnt++ )
	{
		SPI_NRF_RW( *pBuf++ );						//写数据到缓冲区
	}
	NRF_CSN = 1;									//CSN置高
	return( status );
}

u8 SPI_NRF_WriteReg(u8 reg,u8 dat)
{
 	u8 status;
	NRF_CE = 0;										//CE置底
    NRF_CSN = 0;									//CSN置底	
	status = SPI_NRF_RW(reg);						/*发送命令及寄存器号 */	 
    SPI_NRF_RW(dat); 								/*向寄存器写入数据*/   
  	NRF_CSN = 1;			    					//CSN置高	
   	return(status);
}

u8 SPI_NRF_ReadReg(u8 reg)
{
 	u8 reg_val;
	NRF_CE = 0;										//CE置底
    NRF_CSN = 0;									//CSN置底	 
	SPI_NRF_RW(reg); 								/*发送寄存器号*/
	reg_val = SPI_NRF_RW(NOP);						/*读取寄存器的值 */  	
	NRF_CSN = 1;					 /*CSN拉高，完成*/   	
	return reg_val;
}

u8 SPI_NRF_ReadBuf( u8 reg, u8 *pBuf, u8 bytes )
{
 	u8 status, byte_cnt;
	NRF_CE = 0;											//CE置底
	NRF_CSN = 0;										//CSN置底			
	status = SPI_NRF_RW( reg ); 						/*发送寄存器号*/	
	 for(byte_cnt=0; byte_cnt<bytes; byte_cnt++)		/*读取缓冲区数据*/
	 {		  
	     pBuf[byte_cnt] = SPI_NRF_RW(NOP);          //从NRF24L01读取数据
	 }  
	NRF_CSN = 1;									//CSN置高			
 	return status;									//返回寄存器状态值
}

u8 NRF_Rx_Dat(u8 *rxbuf)
{
	u8 state; 
	NRF_CE = 1;											//CE置高	 
	while( NRF_IRQ!=0 );	  							/*等待接收中断*/	
	NRF_CE = 0;											//CE置底               
	state=SPI_NRF_ReadReg( STATUS );						/*读取status寄存器的值  */    
	SPI_NRF_WriteReg( NRF_WRITE_REG+STATUS, state );  		/* 清除中断标志*/ 
	if( state&RX_DR )                                 	//接收到数据
	{
	  SPI_NRF_ReadBuf( RD_RX_PLOAD,rxbuf, RX_PLOAD_WIDTH );//读取数据
	  SPI_NRF_WriteReg( FLUSH_RX, NOP );          			//清除RX FIFO寄存器
	  return RX_DR; 
	}
	else    
		return ERROR;                    				//没收到任何数据
} 

u8 NRF_Tx_Dat( u8 *txbuf )
{
	u8 state;  
	NRF_CE = 0;											//CE置底						
    SPI_NRF_WriteBuf( WR_TX_PLOAD, txbuf, TX_PLOAD_WIDTH );   
 	NRF_CE = 1;											//CE置高                           
	while( NRF_IRQ != 0 ); 		                              
	state = SPI_NRF_ReadReg( STATUS );			   		/*读取状态寄存器的值 */	                   
	SPI_NRF_WriteReg( NRF_WRITE_REG+STATUS, state); 		/*清除TX_DS或MAX_RT中断标志*/	
	SPI_NRF_WriteReg( FLUSH_TX, NOP );    					//清除TX FIFO寄存器  
	if( state&MAX_RT )                     				//达到最大重发次数
			 return MAX_RT; 
	else if( state&TX_DS )                  				//发送完成
		 	return TX_DS;
	else						  
			return ERROR;                 				//其他原因发送失败
}

void NRF_RX_Mode( void )
{
   NRF_CE = 0;												//CE置底	
   SPI_NRF_WriteBuf( NRF_WRITE_REG+RX_ADDR_P0, TX_ADDRESS, RX_ADR_WIDTH );//写RX节点地址
   SPI_NRF_WriteReg( NRF_WRITE_REG+EN_AA, 0x01 );    		//使能通道0的自动应答    
   SPI_NRF_WriteReg( NRF_WRITE_REG+EN_RXADDR, 0x01 );		//使能通道0的接收地址    
   SPI_NRF_WriteReg( NRF_WRITE_REG+RF_CH, CHANAL );      	//设置RF通信频率    
   SPI_NRF_WriteReg( NRF_WRITE_REG+RX_PW_P0, RX_PLOAD_WIDTH );//选择通道0的有效数据宽度      
   SPI_NRF_WriteReg( NRF_WRITE_REG+RF_SETUP, 0x0f ); 		//设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
   SPI_NRF_WriteReg( NRF_WRITE_REG+CONFIG, 0x0f );  		//配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式 
   NRF_CE = 1;												//CE置高
   delay_us( 200 );
} 

void NRF_TX_Mode( void )
{  
   NRF_CE = 0;																//CE置底		
   SPI_NRF_WriteBuf( NRF_WRITE_REG+TX_ADDR, TX_ADDRESS, TX_ADR_WIDTH );    //写TX节点地址 
   SPI_NRF_WriteBuf( NRF_WRITE_REG+RX_ADDR_P0, RX_ADDRESS, RX_ADR_WIDTH ); //设置TX节点地址,主要为了使能ACK   
   SPI_NRF_WriteReg( NRF_WRITE_REG+EN_AA, 0x01 );     		//使能通道0的自动应答    
   SPI_NRF_WriteReg( NRF_WRITE_REG+EN_RXADDR, 0x01 ); 		//使能通道0的接收地址  
   SPI_NRF_WriteReg( NRF_WRITE_REG+SETUP_RETR, 0x1a );		//设置自动重发间隔时间:500us + 86us;最大自动重发次数:10次
   SPI_NRF_WriteReg( NRF_WRITE_REG+RF_CH, CHANAL );   		//设置RF通道为CHANAL
   SPI_NRF_WriteReg( NRF_WRITE_REG+RF_SETUP, 0x0f );  		//设置TX发射参数,0db增益,2Mbps,低噪声增益开启   	
   SPI_NRF_WriteReg( NRF_WRITE_REG+CONFIG, 0x0e );    		//配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,发射模式,开启所有中断
   NRF_CE = 1;				   								//CE置高
   delay_us( 300 );
}

 

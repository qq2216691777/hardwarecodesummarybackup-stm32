/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     工程模板   
			@日期     2015—6-23
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "sd_sdio.h"
#include "ff.h"

FATFS fs;			//记录文件系统盘符信息的结构体    //注意  需要定义为全局变量  也就是在MAIN函数之前定义
int res;			//记录读写文件的返回值
FIL fdst;	    //文件系统结构体，包含文件指针等成员
BYTE textFileBuffer[] = "这是一个FATFS文件系统的测试文件\r\n";
BYTE buffer[512];
int a;
UINT br,bw;

int main( void )
{

	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	if( f_mount( 0, &fs ) == FR_OK )
	{
		printf("注册工作空间成功\n");
	}
	else
	{
		printf("注册工作空间失败\n");
		while(1);
	}
	res = f_open( &fdst,"0:DEMO.txt",FA_CREATE_ALWAYS|FA_WRITE );
	if( res == FR_OK )
	{
		f_write( &fdst, textFileBuffer, sizeof(textFileBuffer), &bw );
		
		f_close( &fdst );
		printf("文件创建成功\n");
	}
	else if( res == FR_EXIST )
	{
		printf("文件已经存在\n");
	}
	else
	{
		printf("文件创建失败\n");
		while(1);
	}
	printf("下面是读出的内容\n");
	
	res = f_open( &fdst, "0:demo.txt", FA_OPEN_EXISTING|FA_READ );
	br=1;
	a=0;
	
	for( a=0; a<512; a++ )
	{
		buffer[a]=0;
	}
	res = f_read( &fdst, buffer, sizeof(buffer), &br );
	printf( "\r\n %s",buffer );
	
	f_close( &fdst );
  f_mount(0, NULL);
	while(1);
}


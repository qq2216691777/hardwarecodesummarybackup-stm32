#ifndef _IIC_SIM__H_
#define _IIC_SIM__H_

#include "stm32f10x.h"
#include "delay.h"


//IO方向设置
#define SDA_IN()  {GPIOC->CRH&=0XFF0FFFFF;GPIOC->CRH|=8<<20;}
#define SDA_OUT() {GPIOC->CRH&=0XFF0FFFFF;GPIOC->CRH|=3<<20;}

//IO操作函数	 
#define IIC_SCL    PBout(9) 	//SCL
#define IIC_SDA    PBout(8)  //SDA	
#define READ_SDA   PBin(8)  //输入SDA

#define	SlaveAddress	0xee		//IIC写入时的地址字节数据，+1为读取

void IIC_Sim_Init( void );
void IIC_Sim_Start(void);		 //I2C起始信号
u8 IIC_Sim_Read_Byte(void);			   //从I2C总线接收一个字节数据

void IIC_Sim_WriteOneData( u8 RegAddr, u8 W_Data );
u8 IIC_Sim_ReadOneReg( u8 RegAddr );
void IIC_Sim_Send_Byte(u8 txd);			//向I2C总线发送一个字节数据

void IIC_Sim_Send_ACK( u8 Ack);		   //I2C发送应答信号
static u8 IIC_Sim_Wait_Ack(void);		   //I2C接收应答信号
void IIC_Sim_Stop(void);			//I2C停止信号

static void IIC_Sim_Delay(void);
static void delayus_nop(u16 t);

#endif


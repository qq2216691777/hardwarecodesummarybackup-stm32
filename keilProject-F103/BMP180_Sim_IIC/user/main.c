/* STM32F10X_HD,USE_STDPERIPH_DRIVER */
/**
	***************************************
			@作者     叶念西风 
			@作用     bmp180模块 调试
			@日期     2015—6-05
			@备注   	模拟IIC
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "iic_sim.h"
#include "bmp180.h"

int main( void )
{
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Bmp180_Init();
	while(1)
	{
		Bmp180_Read_Tem_Pre_Data();
		delay_ms(100);
	}
}


#ifndef _BMP180__H_
#define _BMP180__H_
#include "stm32f10x.h"
#include "iic_sim.h"
#include <math.h>

void Bmp180_Init( void );
long Bmp180_Read_Temp( void );
long Bmp180_Read_Press( void );

long Bmp180_Read_Data( u8 Addr );
long Bmp180_Read3_Data( u8 Addr );
void Bmp180_Read_Tem_Pre_Data( void );

#define OSS 3 							//在Bmp180初始化中设置


#endif

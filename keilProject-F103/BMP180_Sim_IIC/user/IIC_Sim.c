#include "iic_sim.h"

void IIC_Sim_Init( void )
{
		GPIO_InitTypeDef GPIO_InitStructure;
		RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		GPIO_SetBits(GPIOB,GPIO_Pin_9|GPIO_Pin_8); 

		IIC_SCL=1;
		IIC_SDA=1;
}

void IIC_Sim_Start(void)		 //I2C起始信号
{
	SDA_OUT();     //sda线输出
	IIC_SDA=1;
	IIC_SCL=1;	  	  
	
	IIC_Sim_Delay();
 	IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
	IIC_Sim_Delay();
	IIC_SCL=0;//钳住I2C总线，准备发送或接收数据 
}

void IIC_Sim_Send_Byte(u8 txd)			//向I2C总线发送一个字节数据
{                        
    u8 t,temp;   
	  SDA_OUT(); 	    
    for(t=0;t<8;t++)
    {   
			temp = (txd&0x80)>>7;           
					IIC_SDA=temp;
					txd<<=1; 	  
			IIC_Sim_Delay();  
			IIC_SCL=1;
			IIC_Sim_Delay(); 
			IIC_SCL=0;	
			IIC_Sim_Delay();
    }
	IIC_Sim_Wait_Ack();	 
}

u8 IIC_Sim_Read_Byte(void)			   //从I2C总线接收一个字节数据
{
	u8 i,receive=0;
	SDA_OUT();
	IIC_SDA = 1;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{     
		IIC_SCL=1;
		IIC_Sim_Delay();
        receive<<=1;
        if(READ_SDA)receive++;   
		IIC_SCL=0;
		IIC_Sim_Delay();
    }					    
    return receive;
}

void IIC_Sim_Send_ACK( u8 Ack)		   //I2C发送应答信号
{

	IIC_SDA=Ack;
	IIC_Sim_Delay(); 
	IIC_SCL=1;
	IIC_Sim_Delay(); 
	IIC_SCL=0;
	IIC_Sim_Delay(); 	
}
static u8 IIC_Sim_Wait_Ack(void)		   //I2C接收应答信号
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  	   
	IIC_SCL=1;
	IIC_Sim_Delay();	 
	while(READ_SDA)
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Sim_Stop();
			return 1;
		}
	}
	IIC_SCL=0;//时钟输出0 	   
	return 0;  
}

void IIC_Sim_Stop(void)			//I2C停止信号
{
	SDA_OUT();//sda线输出
	IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
  IIC_Sim_Delay();
	IIC_SCL=1;
	IIC_Sim_Delay();
	IIC_SDA=1;//发送I2C总线结束信号
	IIC_Sim_Delay();						   	
}

void IIC_Sim_WriteOneData( u8 RegAddr, u8 W_Data )
{
	IIC_Sim_Start();
	IIC_Sim_Send_Byte(SlaveAddress);	    //发送写命令
	IIC_Sim_Send_Byte(RegAddr);	        //内部寄存器地址
	IIC_Sim_Send_Byte(W_Data);	        //内部寄存器数据
	IIC_Sim_Stop();						//发送停止信号
}


u8 IIC_Sim_ReadOneReg( u8 RegAddr )
{
	u8 Reg_Data;
	IIC_Sim_Start();					//起始信号
	IIC_Sim_Send_Byte(SlaveAddress);	    //发送设备地址+写信号

	IIC_Sim_Send_Byte(RegAddr);	    		//发送存储单元地址，从0开始
	IIC_Sim_Start();
	IIC_Sim_Send_Byte(SlaveAddress+1);	    //发送设备地址+读信号
	Reg_Data = IIC_Sim_Read_Byte();			//读出寄存器数据
	IIC_Sim_Send_ACK(1);					//发送应答信号
	IIC_Sim_Stop();							//发送停止信号
	return Reg_Data;
}

static void IIC_Sim_Delay(void)
{
    volatile int i = 27;
    while (i)
        i--;
}



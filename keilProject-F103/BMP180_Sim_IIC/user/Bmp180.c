#include "bmp180.h"
#include "usart.h"
short AC1;
short AC2; 
short AC3; 
unsigned short AC4;
unsigned short AC5;
unsigned short AC6;
short B1; 
short B2;
short Mb;
short Mc;
short Md;

/******************** Bmp180 初始化 *********************/
void Bmp180_Init( void )
{
	IIC_Sim_Init();
	while( 0x55 != IIC_Sim_ReadOneReg(0xd0) )
	{
		printf("Bmp180 Connect Failed !\n");
		delay_ms(100);
	}
	printf("Bmp180 Connect Success !\n");
	
	AC1 = Bmp180_Read_Data(0XAA);
	AC2 = Bmp180_Read_Data(0XAC);
	AC3 = Bmp180_Read_Data(0XAE);
	
	AC4 = Bmp180_Read_Data(0XB0);
	AC5 = Bmp180_Read_Data(0XB2);
	AC6 = Bmp180_Read_Data(0XB4);
	
	B1 = Bmp180_Read_Data(0XB6);
	B2 = Bmp180_Read_Data(0XB8);
	Mb = Bmp180_Read_Data(0XBA);
	Mc = Bmp180_Read_Data(0XBC);
	Md = Bmp180_Read_Data(0XBE);
	
}


/******************** 读取压强和温度 *******************/
void Bmp180_Read_Tem_Pre_Data( void )
{
	static int i=0;
	static float a_temp[11]={0};
int ii;
	long x1, x2,b5, b6, x3, b3,p;
	unsigned long b4, b7;
	long Ut;
	long Up;
	long  Temperature;
	long  Pressure;
	float Altitude ;//海拔
	
	Ut = Bmp180_Read_Temp();
	Up = Bmp180_Read_Press();

	//温度矫正
	x1 = (Ut - AC6) * AC5/(1<<15);
	x2 = Mc *( 1<<11) / ( x1 + Md );
	b5 = x1+x2;
	Temperature = (b5+8)/(1<<4);	//Temperature*0.1=度
	
	//气压矫正
	/************************************/
	b6 = b5 - 4000;
	x1 = ( B2 * (b6*b6 /(1<< 12)) )/(1<<11);
	x2 = (AC2* b6)/( 1<< 11);
	x3 = x1 + x2;
	b3 = (((AC1 * 4 + x3)<<OSS) + 2)/4;	
	x1 = AC3 * b6 /( 1<<13);
	x2 = (B1 * (b6 * b6 /(1<< 12))) /(1<< 16);
	x3 = ((x1 + x2) + 2) /4;
	b4 = (AC4 * (unsigned long) (x3 + 32768)) /(1<< 15);
	b7 = ((unsigned long) Up - b3) * (50000 >> OSS);
	if( b7 < 0x80000000)
		 p = (b7 * 2) / b4 ;
  else  
		 p = (b7 / b4) * 2;
	x1 = (p /(1<< 8)) * (p /(1<< 8));
	x1 = (x1 * 3038) /(1<<16);
	x2 = (-7357 * p) /(1<<16);
	Pressure = p + ((x1 + x2 + 3791) / 4);//单位Pa  
// 	Pressure=(int)(Pressure/10);
// 	Pressure = Pressure*10;
//海拔计算 不准
	Altitude=(44330.0*(1.0-pow(Pressure/101325.0,1.0/5.255)));
	if(i>9)
		i=0;
	a_temp[i++]=Altitude;
	a_temp[10]=0;
	for(ii=0;ii<10;ii++)
	{
		a_temp[10]+=a_temp[ii];
	}
	printf("P: %4.3f hpa    ",Pressure*0.01 );
	printf("T: %4.2f     ",Temperature*0.1 );
 	printf("A: %4.3f \n",a_temp[10]*0.1 );
// 	printf("T:%ld ",Temperature );
// 	printf("P:%ld \n",Pressure );
}


/******************** 读取内部数据 *********************/
long Bmp180_Read_Data( u8 Addr )
{
	u8 Msb,Lsb;
	short data;
	IIC_Sim_Start();										//起始信号
	IIC_Sim_Send_Byte( SlaveAddress );	//发送设备地址+写信号
	IIC_Sim_Send_Byte( Addr );
	IIC_Sim_Start();										//起始信号
	IIC_Sim_Send_Byte( SlaveAddress+1 );	//发送设备地址+读信号	
	Msb = IIC_Sim_Read_Byte();	
	IIC_Sim_Send_ACK( 0 );
	Lsb = IIC_Sim_Read_Byte();	
	IIC_Sim_Send_ACK( 1 );
	IIC_Sim_Stop();
	data = Msb<<8;
	data |= Lsb;
	return data;
}

long Bmp180_Read3_Data( u8 Addr )
{
	u8 Msb,Lsb,XLsb;
	IIC_Sim_Start();										//起始信号
	IIC_Sim_Send_Byte( SlaveAddress );	//发送设备地址+写信号
	IIC_Sim_Send_Byte( Addr );
	IIC_Sim_Start();										//起始信号
	IIC_Sim_Send_Byte( SlaveAddress+1 );	//发送设备地址+读信号	
	Msb = IIC_Sim_Read_Byte();	
	IIC_Sim_Send_ACK( 0 );
	Lsb = IIC_Sim_Read_Byte();	
	IIC_Sim_Send_ACK( 0 );
	XLsb = IIC_Sim_Read_Byte();
	IIC_Sim_Send_ACK( 1 );
	IIC_Sim_Stop();

	return ((Msb<<16) + (Lsb<<8)+ XLsb)>>(8-OSS);
}
/******************** 读取温度 ***********************/
long Bmp180_Read_Temp( void )
{
	IIC_Sim_Start();										//起始信号
	IIC_Sim_Send_Byte( SlaveAddress );	//发送设备地址+写信号
	IIC_Sim_Send_Byte( 0xF4 );
	IIC_Sim_Send_Byte( 0x2E );
	IIC_Sim_Stop();
	delay_ms(6);
	return (long)Bmp180_Read_Data(0Xf6);
}

/******************** 读取压强 ***********************/
long Bmp180_Read_Press( void )
{
	IIC_Sim_Start();										//起始信号
	IIC_Sim_Send_Byte( SlaveAddress );	//发送设备地址+写信号
	IIC_Sim_Send_Byte( 0xF4 );
	IIC_Sim_Send_Byte( (u8)(0x34 + (OSS<<6)) );
	IIC_Sim_Stop();
		delay_ms(26);
	return Bmp180_Read3_Data(0Xf6);

}



#include <stm32f10x.h>
#include "system_start.h"
#include "uart.h"
#include "nvic.h"

int main( void )
{
	Stm32_Clock_Init( 9 );
	Uart_Init( 9600, 0 );
/********************** T3 定时器 *******************/
	RCC->APB1ENR |= 1<<1;      //打开定时器3
	TIM3->PSC = 7200-1;			   //设置预分频器值
	TIM3->ARR = 4999;            //设置自动重装数值 500ms
	TIM3->DIER |= 1;           //打开更新中断
	TIM3->CR1 |= 1;            //打开计数器
 	NVIC_Init( 1, 3, TIM3_IRQn, 2 );
/********************** T3 定时器 END ****************/
	while(1)
	{	 
	 	
	}
	
}


/***************** T3 中断服务函数 **************/
void TIM3_IRQHandler( void )
{
  	printf("已进入定时器中断\n");
	TIM3->SR &= ~1;
}



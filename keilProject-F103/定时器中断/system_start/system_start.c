#include "system_start.h"

void SystemInit( void )
{
/*********************************** 复位并配置向量眀表 *************************************/	
		RCC->APB1RSTR = 0X00000000;		/* 外设寄存器复位  */
	  RCC->APB2RSTR = 0X00000000;	
	
		RCC->AHBENR = 0X00000014;			/* 使能睡眠模式时的SRAM和闪存 */
		RCC->APB2ENR = 0X00000000;		/* 复位外设时钟 */
	  RCC->APB1ENR = 0X00000000;		
	
		RCC->CR |= 0X00000001;				/*	使能内部高速时钟 */
		RCC->CFGR &= 0XF8FF0000;			/* 复位SW[1:0],HPRE[3:0],PPRE1[2:0],PPRE2[2:0],ADCPRE[1:0],MCO[2:0] */
		RCC->CR &= 0XFEF6FFFF; 				/* 复位HSEON,CSSON,PLLON */
		RCC->CR &= 0XFFFBFFFF;				/* 复位HSEBYP */
		RCC->CFGR &= 0XFF80FFFF;			/* 复位PLLSRC, PLLXTPRE, PLLMUL[3:0] and USBPRE */
		RCC->CIR &= 0X00000000;				/* 关闭所有中断 */
	
  #ifdef VECT_TAB_RAM
				SCB->VTOR = 0X20000000;		/* RAM */
  #else
				SCB->VTOR = 0X80000000;		/* FLASH */
  #endif
/*********************************** 复位并配置向量眀表 end *************************************/
}

void Stm32_Clock_Init( u8 PLL )			/* 复位并配置向量表在SystemInit中 */
{
		u8 temp;
		RCC->CR |= 0X00010000;			/* 使能外部高速时钟 */
		while(!( RCC->CR>>17 ));		/* 等待外部时钟就绪 */
		RCC->CFGR  = 0X00000400;		/* HCLK二分频 */
	
		PLL -=2;
		RCC->CFGR |= PLL << 18;
		RCC->CFGR |= 1 << 16;				/* 设置PLL的倍频数量 */
		FLASH->ACR |= 0X32;					/* FLASH 2个延迟周期 */
		RCC->CR |= 0X01000000;			/* 使能PLL */
		while(!( RCC->CR>>25 ));		/* 等待PLL锁定 */
		RCC->CFGR |= 0X00000002;		/* 设置PLL为系统时钟 */
		while( temp != 0x02 )				/* 等待设置PLL为系统时钟设置成功 */
		{
				temp = RCC->CFGR>>2;
				temp &= 0x03;
		}	
/*********************** Delay ****************************************************/
		SysTick->CTRL &= 0XFFFFFFFB;		/* 设置滴答定时器的来源是 HCLK/8 */
/*********************** Delay ****************************************************/		
}

void delay_us( u32 time )	//72HMZ
{
		u32 temp;
		SysTick->LOAD = 9*time;			/* 时间加载 */
		SysTick->VAL = 0X00;				/* 清空计数器 */
		SysTick->CTRL = 0X01;				/* 使能计数器 */
		do
		{
				temp = SysTick->CTRL;
		}while( temp&0X01 && !( temp & (1<<16 )));
		SysTick->CTRL = 0X00;				/* 关闭计数器 */
		SysTick->VAL = 0X00;				/* 清空计数器 */
}

void delay_ms( u32 time )	//72HMZ
{
		u32 temp;
		SysTick->LOAD = 9*time*1000;			/* 时间加载 */
		SysTick->VAL = 0X00;							/* 清空计数器 */
		SysTick->CTRL = 0X01;							/* 使能计数器 */
		do
		{
			temp = SysTick->CTRL;
		}while( temp&0X01 && !( temp & (1<<16 )));
		SysTick->CTRL = 0X00;							/* 关闭计数器 */
		SysTick->VAL = 0X00;							/* 清空计数器 */
}



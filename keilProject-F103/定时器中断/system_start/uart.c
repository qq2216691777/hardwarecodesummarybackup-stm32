#include "NVIC.h"
#include "uart.h"

u8 USART1_RX_BUF[USART1_RX_LEN];
u16 USART1_RX_STA = 0;

void Uart_Init( u32 Baud, char flag )
{
		float temp = 0;
		u16 Baud_H = 0;
		u16 Baud_L = 0;
		temp = (float)72000000/( Baud*16 );
		Baud_H = temp;
		Baud_L = (int)(( temp - Baud_H )*16);
		Baud_H <<= 4;
		Baud_H += Baud_L;
		RCC->APB2ENR |= 1<<2;			/* 使能A口时钟 TX.PA9复用推挽  RX.PA10 浮空 */
		RCC->APB2ENR |= 1<<14;		/* 使能串口1时钟 */
		
		GPIOA->CRH &= 0XFFFFF00F;	/* 设置IO口模式 9.10 */
		GPIOA->CRH |= 0X000004B0;
	
		RCC->APB2RSTR |= 1<<14;		/*	复位串口1	*/
		RCC->APB2RSTR &= ~(1<<14);
	
		USART1->BRR = Baud_H;			/* 设置波特率 */
		USART1->CR1 |= 0X200C;
	
		USART1->CR1 &= ~(1<<12);
		USART1->CR2 &= 0XCFFF;
		if( flag )
		{
			/* 使能接收  打开中断 */
			USART1->CR1 |= 1<<8;
			USART1->CR1 |= 1<<5;
			NVIC_Init( 3, 3, USART1_IRQn,2 );		/* USART1_IRQn 和 USART1_IRQChannel */	
		}
}

void USART1_IRQHandler( void )
{
		u8 Res = 0;
		if( USART1->SR & (1<<5) )
		{
				Res = USART1->DR;
				if( (USART1_RX_STA & 0X8000)== 0 )		/* 判断上个接收的数据是否结束 */
				{
						if( USART1_RX_STA & 0X4000 )			/* 判断上个数据是否是回车的前半段 */
						{
								if( Res != 0x0a )							/* 判断本次数据是否是回车的后半段 */
								{
											USART1_RX_STA = 0;
								}
								else
								{
										USART1_RX_STA |= 0X8000;
								}
						}
						else
						{
								if( Res == 0x0d )
								{
										USART1_RX_STA |= 0X4000;
								}
								else
								{
											USART1_RX_BUF[USART1_RX_STA&0X3FFF]=Res;
											USART1_RX_STA++;
										  if( USART1_RX_STA>(USART1_RX_LEN-1))
											{
													USART1_RX_STA = 0;
											}
								}							
						}
				}			
		}	
}

/******************** 串口输出用寄存器 ****************/
/*		USART1->DR = 'T'; 						*/
/*		while((USART1->SR&0X40)==0);	*/
/******************** 串口输出用寄存器 *******END******/

/************************ 使用printf函数向串口输出需要加上下面的代码 **************************/
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
	/* Whatever you require here. If the only file you are using is */ 
	/* standard output using printf() for debugging, no file handling */ 
	/* is required. */ 
}; 
/* FILE is typedef’ d in stdio.h. */ 
FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
_sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{      
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
	USART1->DR = (u8) ch;      
	return ch;
}
#endif 
/************************ 使用printf函数向串口输出需要加上下面的代码 ***END**************************/


#ifndef _NVIC_H_
#define _NVIC_H_
#include <stm32f10x.h>

void NVIC_Init( u8 NVIC_Pre, u8 NVIC_Sub, u8 NVIC_Channel, u8 NVIC_Group );
void NVIC_SetGroup( u8 NVIC_Group );

#endif

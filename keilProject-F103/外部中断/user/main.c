#include <stm32f10x.h>
#include "system_start.h"
#include "uart.h"
#include "nvic.h"


int main( void )
{
	Stm32_Clock_Init( 9 );
	Uart_Init( 9600, 0 );

	RCC->APB2ENR |= 0X0170;	//使能CDEG io口
	GPIOD->CRH &= 0XFF0FFFFF;		//D13
	GPIOD->CRH |= 0X00200000;
	GPIOG->CRH &= 0XF0FFFFFF;		//G14
	GPIOG->CRH |= 0X02000000;
	GPIOC->CRH &= 0XFF0FFFFF;	  	//C13 KEY
	GPIOC->CRH |= 0X00800000;
	GPIOE->CRL &= 0XFFFFFFF0;  		//E0  KEY
	GPIOE->CRL |= 0X00000008; 

	RCC->APB2ENR |= 1;	   			//	开启IO口复用时钟
	AFIO->EXTICR[0] &= 0XFFF0;		// 引脚e作为中断输入源
	AFIO->EXTICR[0] |= 0X4;
	AFIO->EXTICR[3] &= 0XFF0F;		// 引脚C作为中断输入源
	AFIO->EXTICR[3] |= 0X20;

	EXTI->IMR |= 0X2001;
	EXTI->FTSR |= 0x2001;
	NVIC_Init( 2, 3, EXTI0_IRQn, 2 ); 
	NVIC_Init( 2, 2, EXTI15_10_IRQn, 2 );

	while(1)
	{
		GPIOD->ODR |= 0X2000;
		delay_ms(500);
		GPIOD->ODR &= ~(0X2000);
		delay_ms(500);
		GPIOD->ODR |= 0X2000;
		delay_ms(500);
		GPIOD->ODR &= ~(0X2000);
		delay_ms(500);
		printf("系统运行中...\n");
	}
	
}

void EXTI0_IRQHandler( void )
{
   delay_ms(20);
   if( (GPIOE->IDR &= 1)==0 )
   {
   		printf("你按下了KEY1 \n");
		while( (GPIOE->IDR &= 1)==0 );
   }
   EXTI->PR |= 1;	 //清除LINE0上的中断标志位 
}

void EXTI15_10_IRQHandler( void )
{
	delay_ms(20);
	if( (GPIOC->IDR &= 0X2000)==0 )
	{
		printf("你按下了KEY2 \n");
		while( (GPIOC->IDR &= 0X2000)==0 );
	}
	EXTI->PR |= 1<<13; //清除LINE0上的中断标志位 
}


/* STM32F10X_HD,USE_STDPERIPH_DRIVER */


/**
  ***************************************
  *@File     main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@brief              
  *@HAUST - RobotLab
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "ili9320.h"

int main( void )
{
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Ili9320_Init();  
	LCD_Clear( White );
	GUI_Text( 50,80,"Zhou wangsheng", Black, White );
	GUI_Word(50,120,5,589,1,Black,White);
//	GUI_Chinese_Text(0,140,"周旺生",Black,White);
	while(1)
	{

	}
}

